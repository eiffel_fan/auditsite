<%@ Page Language="C#" MasterPageFile="~site/_catalogs/masterpage/ThemeDefault.master" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage,Microsoft.SharePoint,Version=16.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:progid="SharePoint.WebPartPage.Document" meta:webpartpageexpansion="full" %>

<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
	文档编辑
</asp:Content>

<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">
	<style>
		.col-sm-2.control-label.upl {
		    text-align: right;
		    width: 161px;
		    margin-right: -30px;
		    color: #242424;
		    padding-right: 15px;
		    padding-left: 15px;
		}
		.form-group.clear_fix .col-sm-6{
		    padding-right: 15px;
		    padding-left: 15px;
		}
		.col-sm-2.control-label.upl.doc{
		    margin-right:-13px;
		}
		.col-sm-8{
		    margin-top: 7px;
		    padding-left: 0px;
		    text-align: initial;
		}
		.col-sm-8 p{
				font-size: 12px;
		    color: #6e6e6e;
		    line-height: 25px;
		}
		.upfj{
		    margin-top: 8px;
		}
		.inline{
				text-align: right;
		    width: 161px;
		    padding-right: 15px;
		    padding-left: 15px;
		    float: left;
		}
		#txtKMTag div{
			width: 50%;
			display: inline-block;
		}
		.hyperlink{
			color: rgb(51, 122, 183);
			cursor: pointer
		}
		.hyperlink:hover{
			color: rgb(35, 82, 124);
		}
		#divFileAttachment>div{
			margin-left: 151px;
		}
		.fade.in{
			display: flex!important;
		}
	</style>

	<script src="../SiteAssets/jQuery.spHtmlEditor.js" ></script>
	<script type="text/javascript">
			var siteUrl = _spPageContextInfo.webAbsoluteUrl;
      var listId = "";
      var spItemId = 0;
      var dimensionObj = {};
      var lstKMAttachment = new Array();
      var lstKMDimension = new Array();
      var knowledge = new Object();
      var listTitle = "";
      var isSiteOwner = false;
      var lstSiteOwnerLoginName = new Array();
      var selectdimension = "请选择维度分类";
      var langSelectpeople = "人员选择";
      var langListID = "获取ListID失败！";
      var langknowledge = "创建条目成功，但获取附件属性出错！";
      var langobtained = "未获取到相关记录！";
      var langalready = "已选择了该维度, 同一维度不可被多次选择！";
      var langFailedtoobtainknowledge = "获取失败！";
      var langFailedtoload = "加载失败！";
      var langprimary = "请选择主维度";
      var langagain = "主文件已存在，请删除已有的主文件后重试！";
      var langMB = "文件大小不能超过50MB";
      var langPleasetryagain = "文件重复请重试";
      var langattachments = "删除附件失败!";
      var langerror = "刪除附件出現錯誤！";
      var langtitle = "请输入文档标题！";
      var langcharacters = "文档标题不能超过50个字符";
      var langknowledgedimension = "请选择知识维度！";
      var langmaster = "请上传主文档！";
      var languploadknowledge = "上传知识失败！";
      var languploadfile = "上传文件失败！";
      var langFailedtosave = "保存失败！";
      var langsuccessfully = "上传知识成功，但设置权限出错！";
      var perViewOnly = "View Only";
      var lcid = SP.Res.lcid;
			var hasBeenLocked = false;
      switch (lcid) {
	            case "1033":
	                selectdimension = "Please select a dimension";
	                langSelectpeople = "Select people";
	                langListID = "Failed to obtain ListID";
	                langknowledge = "Created knowledge item successfully, but failed to obtain attachment attributes!";
	                langobtained = "No related record obtained";
	                langalready = "This dimension is already selected and cannot be selected multiple times";
	                langFailedtoobtainknowledge = "Failed to obtain knowledge!";
	                langFailedtoload = "Failed to load!";
	                langprimary = "Please select the primary dimension";
	                langagain = "The master file already exists. Please delete the existing master file and try again!";
	                langMB = "File size should not exceed 50 MB";
	                langPleasetryagain = "The file is duplicated. Please try again";
	                langattachments = "Failed to delete attachments!";
	                langerror = "An error occurs when the attachment is being deleted!";
	                langtitle = "Please enter the document title!";
	                langcharacters = "Document title should not exceed 50 characters";
	                langknowledgedimension = "Please select the knowledge dimension!";
	                langmaster = "Please upload the master file of knowledge";
	                languploadknowledge = "Failed to upload knowledge!";
	                languploadfile = "Failed to upload file!";
	                langFailedtosave = "Failed to save";
	                langsuccessfully = "Uploaded knowledge successfully, but failed to set permission!";
	                //perViewOnly = "View Only";
	                break;
	            case "1028":
	                selectdimension = "請選擇維度分類";
	                langSelectpeople = "人員選擇";
	                langListID = "獲取ListID失敗！";
	                langknowledge = "創建知識條目成功，但獲取附件屬性出錯！";
	                langobtained = "未獲取到相關記錄！";
	                langalready = "已選擇了該維度，同一維度不可被多次選擇！";
	                langFailedtoobtainknowledge = "獲取知識失敗！";
	                langFailedtoload = "加載失敗！";
	                langprimary = "請選擇主維度";
	                langagain = "主文件已存在，請刪除已有的主文件后重試！";
	                langMB = "文件的大小不能超過50MB";
	                langPleasetryagain = "文件重複請重試";
	                langattachments = "刪除附件失敗！";
	                langerror = "刪除附件出現錯誤！";
	                langtitle = "請輸入文檔標題！";
	                langcharacters = "文檔標題不能超過50個字符";
	                langknowledgedimension = "請選擇知識維度！";
	                langmaster = "請上傳知識主文檔！";
	                languploadknowledge = "上傳知識失敗！";
	                languploadfile = "上傳文件失敗！";
	                langFailedtosave = "保存失敗！";
	                langsuccessfully = "上傳知識成功，但設置權限出錯！";
	                //perViewOnly = "僅檢視";
	                break;
	        }

			console.log(siteUrl);

			var currentDocVersion;
			var approvedStatus;
			var knowledgeType;
			var mainFileExist = false, tempFileExist = false, isMainAttachmentMod = true;
			var tempFileName = "", mainFileName2 = "";
			var chooseAttachment;
			var cat0, cat1, cat2;
			var tags;
			var lstSelectedUser4ViewOnly = new Array();
	    var lstSelectedUser4Editable = new Array();
	    var lstSelectedUser4Download = new Array();
			var config = new Array();
			var deleteFileQ = new Array();
			var currentuserDisplayName;
	    var privilegeOption = 0;
			// var ContentTypeId = getSearchParams('ContentTypeId');
			var spItemId = getSearchParams('cid');
			(function($) {
			   ExecuteOrDelayUntilScriptLoaded(function(){
			      $(function() {
			        $("#rteeditor").spHtmlEditor();
			      });
			   },'sp.js');
			 })(jQuery);
		 	dataInti();
			$(document).ready(function(){
	 	 		$("#rteeditor_editor").html("<br/><br/><br/><br/>");
			});

			async function dataInti(){
				await get_config();
				initUserInfo();
			 	getLoginNamePrefix();
				getTagChoice();
			 	if(spItemId)
					loadKnowledge(spItemId);
				else
					get_lookupValue('O_Class');
			}

			function swapCurrentDocVersion(curr) {
				return (parseInt(curr) + 1) % 2;
			}

			function mainItemName(curr) {
				return parseInt(curr) == 0 ? 'MAIN' : 'MAIN2';
			}

			function filePrefix(curr) {
				return '[' + mainItemName(curr) + ']';
			}

			function currentFilePrefix() {
				return filePrefix(currentDocVersion);
			}

			function get_config(){
		    return $.ajax({
		      url: String.format("{0}/_api/web/lists/getByTitle('app.Config')/items",
		          siteUrl),
		      type: "GET",
		      headers: {
		        'content-type': 'application/json;odata=verbose',
		          "accept": "application/json;odata=verbose",
		          "X-RequestDigest": $("#__REQUESTDIGEST").val()
		      },
		      success: function (data) {
		        config = data.d.results;
		        listTitle = config.filter(x=> x.Title == "MainListName")[0].Data;
						if(config.filter(x=> x.Title == "First_Class")[0].Data == "Yes")
							$("#cat1").show();
						if(config.filter(x=> x.Title == "Second_Class")[0].Data == "Yes")
							$("#cat2").show();
		      },
		      error: function (error) {
		        console.log(error);
		      }
		    });
		  }

			function backToList(){
				var folderUrl = String.format("{0}/Lists/{1}", siteUrl, listTitle);
				// $("#submit").click(function() {
				// 	location.href= folderUrl+'/AllItems.aspx';
				// })

					location.href= folderUrl+'/AllItems.aspx';

			}

			function initUserInfo() {
					var context = new SP.ClientContext.get_current();
					this.website = context.get_web();
					this.currentUser = website.get_currentUser();
					context.load(currentUser);
					context.executeQueryAsync(Function.createDelegate(this, this.onQueryUserSucceeded), Function.createDelegate(this, this.onQueryUserFailed));
			}

			function onQueryUserSucceeded(sender, args) {
					currentuserDisplayName = currentUser.get_title();
					console.log(currentuserDisplayName);
			}

			function onQueryUserFailed(sender, args) {}

			function mainItemName(currentActive) {
				return currentActive == 0 ? 'MAIN' : 'MAIN2';
			}

			function getAttachmenFile(rawTxt, currentActive){

				$.getJSON(siteUrl+"/_api/lists/getByTitle('"+listTitle+"')/items("+spItemId+")?$select=AttachmentFiles,Title&$expand=AttachmentFiles",
	    	function(data) {

					 var mainDocName = currentFilePrefix();
					 var tempDocName = filePrefix(swapCurrentDocVersion(currentDocVersion));
					// if(data.AttachmentFiles.length > 0 && data.AttachmentFiles[0].FileName.indexOf('['+mainDocName+']') >= 0){
						// $("#contentSelect")[0].options[0].selected = true;
				 		// $("#mainFileDiv").show();
				 		// $("#contentSelector").hide();
					// }
					// else {
						// $("#contentSelect")[0].options[1].selected = true;
				 		// $("#mainFileDiv").hide();
				 		// $("#contentSelector").show();
					// }
					 for(var i = 0; i < data.AttachmentFiles.length; i++){
						 var fileName = data.AttachmentFiles[i].FileName;
						 var fileType = data.AttachmentFiles[i].FileName.slice(data.AttachmentFiles[i].FileName.lastIndexOf(".")+1);
						 var identity = ((new Date().getTime()) + i + 1).toString();
						 if (data.AttachmentFiles[i].FileName.indexOf(tempDocName) >= 0) {
							 tempFileExist = true;
							 tempFileName = data.AttachmentFiles[i].FileName;
							 if (!isMainAttachmentMod) {
							 	deleteFileQ.push(tempFileName);
							}
							 continue;
						 }

						 var str = "<div class=\"fj_connent\" id='fileDiv" + identity + "''>";

						 if (fileType == "doc" || fileType == "docx")
								 str += "<img src='../Style Library/LKKHPG/images/index/ico-type-doc.png' />";
						 else if (fileType == "def")
								 str += "<img src='../Style Library/LKKHPG/images/index/ico-type-def.png' />";
						 else if (fileType == "pdf")
								 str += "<img src='../Style Library/LKKHPG/images/index/ico-type-pdf.png' />";
						 else if (fileType == "ppt" || fileType == "pptx")
								 str += "<img src='../Style Library/LKKHPG/images/index/ico-type-ppt.png' />";
						 else if (fileType == "xls" || fileType == "xlsx")
								 str += "<img src='../Style Library/LKKHPG/images/index/ico-type-xls.png' />";
						 else
								 str += "<img src='../Style Library/LKKHPG/images/index/ico-type-doucment.png' />";

						 if (data.AttachmentFiles[i].FileName.indexOf(mainDocName) >= 0) {
							 str += "&nbsp;&nbsp;<span class='filename' title=" + fileName + ">" + fileName + "</span>";
							 mainFileExist = true;
							 mainFileName2 = fileName;
							 chooseAttachment = true;
						 } else {
						 	 str += "<input type='text' id='" + identity + "' value='" + fileName + "' style='height:25px'  name='attachment' title=" + fileName.replace(/ /g, '_') + ">";
						 }

						 str += "&nbsp;&nbsp;<span class='rliang'>" + " " + "</span>";
						 str += "<a style=\"cursor: pointer\" onclick=\"execDeleteAttachment('" + identity + "', '" + data.AttachmentFiles[i].FileName + "', '')\">"
						 str += "<img src=\"../Style Library/LKKHPG/images/index/ico-delete3.png\" />";
						 str += "</a>";
						 str += "</div>";

						var gpName;
						if(rawTxt){
							var splited = rawTxt.split(";;");
		 					for(var r = 0; r < splited.length-1; r++){
		 						if(splited[r].split("<,>")[1] == data.AttachmentFiles[i].FileName){
		 							gpName = splited[r].split("<,>")[2];
		 						}
		 					}
						}

						 if (data.AttachmentFiles[i].FileName.indexOf(currentFilePrefix()) >= 0) {
								 $("#divFileMain").html(str);
						 } else {
								 $("div[name='"+gpName+"'] #divFileAttachment").append(str);
						 }
					}

					 //rename function
					if(rawTxt){
		 				$("#readingMaterial").html('');
		 				// var splited = rawTxt.split(";;");
						// console.log(splited[0].split("<,>")[1]);
		 				for(var i = 0; i < splited.length-1; i++){
		 					if(splited[i].split("::")[0] == 'docName')
		 						$("input[title='"+splited[i].split("::")[1].split("<,>")[1]+"']").val(splited[i].split("::")[1].split("<,>")[0]);
		 					else if(splited[i].split("::")[0] == 'name'){
		 						var identity = (new Date().getTime() + i + 1).toString();
		 						var str = $("div[name='"+splited[i].split("::")[1].split("<,>")[2]+"'] #readingMaterial").html()
		 						str += '<div id="'+identity+'" style="margin-bottom: 10px;"><span class="inline">名称 ： </span><input type="text" class="urlName" value="'+splited[i].split("::")[1].split("<,>")[0]+'"> - <span>链接 : </span><input type="text" class="urlLink" value="'+splited[i].split("::")[1].split("<,>")[1]+'"> <span style="color: red; cursor: pointer;" onclick="deleteReadingMaterial('+identity+')">  X  </span></div>';
		 						$("div[name='"+splited[i].split("::")[1].split("<,>")[2]+"'] #readingMaterial").html(str);
		 					}
		 				}
		 			}
				})
			}

			function deleteAttachment(){
				console.log("deleteFileQ",deleteFileQ);
				if(deleteFileQ.length > 0){
					var fileName = deleteFileQ.pop();
					var ctx = SP.ClientContext.get_current();
					var list = ctx.get_web().get_lists().getByTitle(listTitle);
					var item = list.getItemById(spItemId);
					var attachmentFile = item.get_attachmentFiles().getByFileName(fileName);
					attachmentFile.deleteObject();
					ctx.executeQueryAsync(
					  function(){
					     console.log('Attachment file has been deleted');

							 if(deleteFileQ.length > 0){
								 console.log(deleteFileQ);
								 deleteAttachment();
							 }
					  },
					  function(sender,args)
					  {
							 alert(langattachments);
							 location.reload();
					  });
					}
			}

			function loadKnowledge(spItemId) {
				console.log('Start loadKnowledge - Id: '+spItemId);
	      var clientContext = new SP.ClientContext.get_current();
	      var oList = clientContext.get_web().get_lists().getByTitle(listTitle);

	      this.listItem = oList.getItemById(spItemId);
	      clientContext.load(listItem);

	      clientContext.executeQueryAsync(
					Function.createDelegate(this, this.updatingForm),

					Function.createDelegate(this, this.onQueryFailed)
				);
	    }

	    function updatingForm(sender, args){

	      var title = this.listItem.get_item("Title");
	      var Oclass = this.listItem.get_item("O");
	      var first = this.listItem.get_item("First");
	      var Second = this.listItem.get_item("Second");
	      var target = this.listItem.get_item("Target")
				var tag = this.listItem.get_item("Tag");
				var approvedStatus = this.listItem.get_item('_ModerationStatus');
				//var hasModified = this.listItem.get_item
				var _Content = this.listItem.get_item("_Content");
				var attachmentGroup = this.listItem.get_item("attachmentGroup");

				var rawTxt = this.listItem.get_item("readingMaterials");
				var knowledgeType = this.listItem.get_item("KnowledgeType");
				var thumbnailData = this.listItem.get_item("ThumbnailData");
				isMainAttachmentMod = this.listItem.get_item('IsMainAttachmentMod');

			  approvedStatus = this.listItem.get_item('_ModerationStatus');
			  currentDocVersion = this.listItem.get_item("CurrentActiveDoc");
			  //knowledgeType = this.listItem.get_item("KnowledgeType");
				if (currentDocVersion == null) {
					currentDocVersion = 0;
				}

				console.log('Approval Status: ' + approvedStatus);

				if (approvedStatus == 0) {
					isMainAttachmentMod = false;
				}

	      $("#txtKMTitle").val(title);
				if(tag!=null){
				tags = tag;
					tag.forEach(function(checkedTagValue){
						$("#txtKMTag div input[value='"+checkedTagValue+"']").prop('checked', true);
					})
				}
				// $("#rteeditor").spHtmlEditor({method: "sethtml", html: _Content});
				$("#rteeditor_editor").html(_Content);
				if(Oclass){
					cat0 = Oclass.get_lookupId();
					// $("#category0").val(cat0);
				}
				if(first) {
					cat1 = first.get_lookupId();
					// $("#category1").val(cat1);
				}
				if(Second) {
					cat2 = Second.get_lookupId();
					// $("#category2").val(cat2);
				}

				if(target){
			    for(var i = 0; i < target.length; i++){
			      $("#txtViewOnly").val($("#txtViewOnly").val()+target[i].get_lookupValue());
			      if(i!=target.length-1)
			        $("#txtViewOnly").val($("#txtViewOnly").val()+',');

					getEmail(target[i]);
			    }
				}

				if (knowledgeType=="attachment") {
					$("#contentSelect")[0].options[0].selected = true;
					$("#mainFileDiv").show();
					$("#attachment").show();
					$(".videoAtt").hide();
					$("#contentSelector").hide();
				}
				else if (knowledgeType=="video") {
					$("#contentSelect")[0].options[2].selected = true;
					$("#mainFileDiv").show();
					$("#attachment").hide();
					$(".videoAtt").show();
					$("#contentSelector").hide();
				}
				else {
					$("#contentSelect")[0].options[1].selected = true;
					$("#mainFileDiv").hide();
					$("#attachment").hide();
					$(".videoAtt").hide();
					$("#contentSelector").show();
				}

				if(thumbnailData){
					base64ToImg(thumbnailData);
				}

				$(".attachmentGroup").html('');
				if(attachmentGroup){
					attachmentGroup = attachmentGroup.split("<,>");
					// $(".attachmentGroup").html('');
					for(var i = 0; i < attachmentGroup.length-1; i++){
						addNewAttGp(attachmentGroup[i]);
						$(".attachmentGroup>div")[i].getElementsByClassName('attachmentGroupName')[0].value = attachmentGroup[i];
					}
				}
				get_lookupValue('O_Class');
		    getAttachmenFile(rawTxt);
	    }

			function getTagChoice(){
				var clientContext = new SP.ClientContext.get_current();
	      var oList = clientContext.get_web().get_lists().getByTitle(listTitle);
				var choiceField = clientContext.castTo(oList.get_fields().getByInternalNameOrTitle("Tag"),SP.FieldChoice);
				clientContext.load(choiceField);
				clientContext.executeQueryAsync(function(){
					var choices = choiceField.get_choices();
	    		// console.log("Choices: (" + choices.length + ") - " + choices.join(", "));
					// console.log(choices);
					choices.forEach(function(choice){
						$("#txtKMTag").html($("#txtKMTag").html()+"<div><input type='checkbox' name='tag' value='"+choice+"' style='margin:9px'><span>"+choice+"</span></div>");
					});
					if (tags) {
						tags.forEach(function(checkedTagValue){
							$("#txtKMTag div input[value='"+checkedTagValue+"']").prop('checked', true);
						});
					}
				},
	        function (){
						console.error('error on getting choices');
					});
			}

			function getLoginNamePrefix() {
			    var context = new SP.ClientContext.get_current();
			    var web = context.get_web();
			    // var user = web.get_siteUsers().getByLoginName(userName);
					var user = web.get_siteUsers().getById(1);
			    context.load(user);

			    loginNamePrefix = context.executeQueryAsync(function() {

						var prefix = user.get_loginName();

						$(".choose_tip").attr('id',prefix.split("|")[0]+"|"+prefix.split("|")[1]+"|");

			    }, function() {
			        console.log("Prefix Not Found");
			    });
			}

			function getEmail(userId) {
			    var context = new SP.ClientContext.get_current();
			    var web = context.get_web();
			    var user = web.get_siteUsers().getById(userId.get_lookupId());
			    context.load(user);

			    context.executeQueryAsync(function() {
			    // console.log(user);
						var privilege = {};

						if (userId.get_email().indexOf('@') != -1) { //包含@是用户，其他是组织
								privilege.EmailAddress = userId.get_email();
								privilege.ReceiverName = userId.get_lookupValue();
								privilege.Type=1;
								privilege.Code = userId.get_email();
								privilege.ID = "";
						} else {
								privilege.EmailAddress = "";
								privilege.ReceiverName = userId.get_lookupValue();
								privilege.Code = user.get_loginName().split("|")[2].toUpperCase();
								privilege.Type = 0;
								privilege.ID = "";
						}

						lstSelectedUser4ViewOnly.push(privilege);
			    }, function() {
			        console.log("User Not Found");
			    });
			}

			function get_lookupValue(title){
					var clientContext = new SP.ClientContext.get_current();
					var oList = clientContext.get_web().get_lists().getByTitle(title);

					var camlQuery = new SP.CamlQuery();

					this.item = oList.getItems(camlQuery);
					clientContext.load(item);

					if(title=='O_Class')
						clientContext.executeQueryAsync(
								Function.createDelegate(this, this.onLookUpQuerySucceeded),
								Function.createDelegate(this, this.onQueryFailed)
						);
					else if(title=='First_Class'){
							clientContext.executeQueryAsync(
									function(){
										var listItemEnumerator = item.getEnumerator();
										$('#category1').html('');
						 			 	while (listItemEnumerator.moveNext()) {
											try{
						 						var oListItem = listItemEnumerator.get_current();
												if(oListItem.get_item('O_Class').get_lookupId()==$("#category0")[0].options[category0.selectedIndex].value){
													$('#category1').append($('<option>', {
														value: oListItem.get_item('ID'),
														text: oListItem.get_item('Title')
													}));
												}
											} catch(err){}
										}
										$('#category1').val(cat1);

										get_lookupValue('Second_Class');
									},
									function(){}
							);
						}
					else{
						clientContext.executeQueryAsync(
								function(){
									var listItemEnumerator = item.getEnumerator();
									if(title=='Second_Class'){
										$('#category2').html('');
									}
					 			 	while (listItemEnumerator.moveNext()) {
										try{
						 					var oListItem = listItemEnumerator.get_current();
											if(oListItem.get_item('First_Class').get_lookupId()==$("#category1")[0].options[category1.selectedIndex].value){
												$('#category2').append($('<option>', {
													value: oListItem.get_item('ID'),
													text: oListItem.get_item('Title')
												}));
											}
										} catch(err){}
									}

										$('#category2').val(cat2);

								},
								function(){}
						);}
			}

			function onLookUpQuerySucceeded(sender, args){

					 var listItemEnumerator = item.getEnumerator();

					 while (listItemEnumerator.moveNext()) {
						 try{
							 var oListItem = listItemEnumerator.get_current();

								$('#category0').append($('<option>', {
									value: oListItem.get_item('ID'),
									text: oListItem.get_item('Title')
								}));
							 // console.log(oListItem.get_item('ID') + ' : ' + oListItem.get_item('Title'));
							 // console.log(oListItem.get_item('First_Class').get_lookupValue() + ' : ' + oListItem.get_item('First_Class').get_lookupId());
						 } catch(err){}
					 }
					 if (cat0) {
					 	$('#category0').val(cat0);
					 }
			     if (spItemId) {
			 				// loadKnowledge(spItemId);
			 		  } else {
			 				spItemId = 0;
			      }
				 	 	get_lookupValue('First_Class');
					}

			function getSearchParams(k){
				var p={};
				location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(s,k,v){p[k]=v})
				return k?p[k]:p;
			}

			function showSelectUserDialog(value) {
			  privilegeOption = value;
			  var urlSelectUser = String.format("/_layouts/15/LKKHPG.Common/UserSelector.aspx?selectModel={0}&loginUser={1}&lcid={2}", 0, _spPageContextInfo.userLoginName, lcid);

			  var lstSelected = new Array();
			  if (value == 0) {
	      	for (idx = 0; idx < lstSelectedUser4ViewOnly.length; idx++) {
	          var item = "";
	          var EmailAddress = lstSelectedUser4ViewOnly[idx].EmailAddress;
	          //用户
	          if (EmailAddress.indexOf('@') != -1) {//包含@是用户，其他是组织
	              item =
	                  {
	                      Type: 1,
	                      Code:  lstSelectedUser4ViewOnly[idx].Code,
	                      Name: lstSelectedUser4ViewOnly[idx].ReceiverName,
	                      Email: lstSelectedUser4ViewOnly[idx].EmailAddress,
	                      ID: lstSelectedUser4ViewOnly[idx].ID
	                  };
	          } else {
	              //部门
	              item =
	                  {
	                      Type: 0,
	                      Code: lstSelectedUser4ViewOnly[idx].Code,
	                      Name: lstSelectedUser4ViewOnly[idx].ReceiverName,
	                      Email: '',
	                      ID: lstSelectedUser4ViewOnly[idx].ID
	                  };
	          }

	          lstSelected.push(item);
	      	}
	  		}
				else if (value == 1) {
		      for (idx = 0; idx < lstSelectedUser4Editable.length; idx++) {
		          var item = "";
		          //如果是用户包含@
		          var EmailAddress = lstSelectedUser4Editable[idx].EmailAddress;
		          //用户
		          if (EmailAddress.indexOf('@') != -1) {//包含@是用户，其他是组织
		              item =
		              {
		                  Type: 1,
		                  Code: lstSelectedUser4Editable[idx].EmailAddress,
		                  Name: lstSelectedUser4Editable[idx].ReceiverName,
		                  Email: lstSelectedUser4Editable[idx].EmailAddress,
		                  ID: lstSelectedUser4Editable[idx].ID
		              };
		          } else {
		              item =
		              {
		                  Type: 0,
		                  Code: lstSelectedUser4Editable[idx].Code,
		                  Name: lstSelectedUser4Editable[idx].ReceiverName,
		                  Email: '',
		                  ID: lstSelectedUser4Editable[idx].ID
		              };
		          }

		          lstSelected.push(item);
		      }
	  		}
				else {
		      for (idx = 0; idx < lstSelectedUser4Download.length; idx++) {
		          var item = "";
		          var EmailAddress = lstSelectedUser4Download[idx].EmailAddress;
		          //用户
		          if (EmailAddress.indexOf('@') != -1) {//包含@是用户，其他是组织
		              item =
		                  {
		                      Type: 1,
		                      Code: lstSelectedUser4Download[idx].Code,
		                      Name: lstSelectedUser4Download[idx].ReceiverName,
		                      Email: lstSelectedUser4Download[idx].EmailAddress,
		                      ID: lstSelectedUser4Download[idx].ID
		                  };
		          } else {
		              //部门
		              item =
		                  {
		                      Type: 0,
		                      Code: lstSelectedUser4Download[idx].Code,
		                      Name: lstSelectedUser4Download[idx].ReceiverName,
		                      Email: '',
		                      ID: lstSelectedUser4Download[idx].ID
		                  };
		          }

		          lstSelected.push(item);
		      }
	  		}

		    var options = {
		        url: urlSelectUser,
		        args: lstSelected,
		        title: langSelectpeople,
		        showClose: false,
		        dialogReturnValueCallback: dialogCallback
		    };
		    SP.SOD.executeFunc(
		     'sp.ui.dialog.js',
		     'SP.UI.ModalDialog.showModalDialog',
		     function () {
		         SP.UI.ModalDialog.showModalDialog(options);
		     });
	    }

	    function dialogCallback(dialogResult, returnValue) {

	    	if (dialogResult == SP.UI.DialogResult.OK) {
	        var lstSelectedObject = returnValue;

	        if (privilegeOption == 0)
	            lstSelectedUser4ViewOnly = new Array();
	        else if (privilegeOption == 1)
	            lstSelectedUser4Editable = new Array();
	        else
	            lstSelectedUser4Download = new Array();

	        if (lstSelectedObject != null) {
	            for (idx = 0; idx < lstSelectedObject.length; idx++) {
	                var newMember = "";
	                var EmailAddress = lstSelectedObject[idx].Email;
	                //用户
	                if (EmailAddress.indexOf('@') != -1) {//包含@是用户，其他是组织
	                    newMember =
	                    {
	                        ID: lstSelectedObject[idx].ID,
	                        Code: lstSelectedObject[idx].Email,
	                        Type:1,
	                        ReceiverName: lstSelectedObject[idx].Name,
	                        EmailAddress: lstSelectedObject[idx].Email
	                    };
	                } else {
	                    //部门
	                    newMember =
	                    {
	                        ID: lstSelectedObject[idx].ID,
	                        Code: lstSelectedObject[idx].Code,
	                        Type: 0,
	                        ReceiverName: lstSelectedObject[idx].Name,
	                        EmailAddress: lstSelectedObject[idx].Email
	                    };
	                }

	                if (privilegeOption == 0)
	                    lstSelectedUser4ViewOnly.push(newMember);
	                else if (privilegeOption == 1)
	                    lstSelectedUser4Editable.push(newMember);
	                else
	                    lstSelectedUser4Download.push(newMember);
	            }
							// console.log(lstSelectedUser4ViewOnly);
	        }

	        var lstUserName = new Array();

	        if (privilegeOption == 0) {
	            for (i = 0; i < lstSelectedUser4ViewOnly.length; i++) {
	                lstUserName.push(lstSelectedUser4ViewOnly[i].ReceiverName);
	            }
	            $("#txtViewOnly").val(lstUserName.join(", "));
	        }
	        else if (privilegeOption == 1) {
	            for (i = 0; i < lstSelectedUser4Editable.length; i++) {
	                lstUserName.push(lstSelectedUser4Editable[i].ReceiverName);
	            }
	            $("#txtEditable").val(lstUserName.join(", "));
	        }else {
	            for (i = 0; i < lstSelectedUser4Download.length; i++) {
	                lstUserName.push(lstSelectedUser4Download[i].ReceiverName);
	            }
	            $("#txtDownloadable").val(lstUserName.join(", "));
	        }
		    }
			}

	    function lockActionButtons(lockOrNot) {
	        hasBeenLocked = lockOrNot;
	        $(".delete-file").attr({ "disabled": lockOrNot });
	        $("#btnSave").attr({ "disabled": lockOrNot });
	        $("#btnCancel").attr({ "disabled": lockOrNot });
	    }

			function bytesToSize(bytes) {
				const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']
				if (bytes === 0) return 'n/a'
					const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10)
				if (i === 0) return `${bytes} ${sizes[i]})`
					return `${(bytes / (1024 ** i)).toFixed(1)} ${sizes[i]}`
			}

	    function popupFileChooser(isMainFile, gpName) {
	      if (isMainFile == 1 && $("#divFileMain").html().trim().length > 0) {
	          alert(langagain);
	          return;
	  		}

	      var identity = new Date().getTime().toString();
	      var strHtml = "<input style='display: none' type='file' id='filePicker" + identity + "' name='" + identity + "' file-name='' file-type='' file-main='' file-uploaded='false' class='selected-file' onchange=\"onSelectFileChanged('" + identity + "', '" + isMainFile + "', '" + gpName + "')\" />";
	      $("#divSelectFile").append(strHtml);
	      $("#filePicker" + identity).click();
	  	}

			function thumbnailChooser(){
				var strHtml = '<input type="file" style="display: none" id="thumbnail" onchange="encodeImageFileAsURL(this)" />';
				$("#divSelectFile").append(strHtml);
	      $("#thumbnail").click();
			}

	    function onSelectFileChanged(identity, isMainFile, gpName) {
	        var fileInput = document.getElementById("filePicker" + identity);
	        var file = fileInput.files[0];
	        if (file != null) {
	            //文件最大不超过50M
	            if (file.size > 52428800) {
	                alert(langMB);
	                $("#filePicker" + identity).remove();
	                return;
	            }

	            var fileName = file.name;
	            var fileSize = bytesToSize(file.size);

	            var parts = file.name.split('.');
	            var fileType = parts[parts.length - 1];

	            var isDuplicated = false;

	            $.each($("#divFileMain").find("span"), function (i, item) {
	                var text = $(item).text();
	                if (text == fileName || text == currentFilePrefix() + fileName) {
	                    isDuplicated = true;
	                    return;
	                }
	            });

	            if (isDuplicated == false) {
	                $.each($("#divFileAttachment").find("span"), function (i, item) {
	                    var text = $(item).text();
	                    if (text == fileName) {
	                        isDuplicated = true;
	                        return;
	                    }
	                });
	            }

	            if (isDuplicated == true) {
	                alert(langPleasetryagain);
	                $("#filePicker" + identity).remove();
	                return;
	            }

	            var str = "<div class=\"fj_connent\" id='fileDiv" + identity + "'>";

	            if (fileType == "doc" || fileType == "docx")
	                str += "<img src='../Style Library/LKKHPG/images/index/ico-type-doc.png' />";
	            else if (fileType == "def")
	                str += "<img src='../Style Library/LKKHPG/images/index/ico-type-def.png' />";
	            else if (fileType == "pdf")
	                str += "<img src='../Style Library/LKKHPG/images/index/ico-type-pdf.png' />";
	            else if (fileType == "ppt" || fileType == "pptx")
	                str += "<img src='../Style Library/LKKHPG/images/index/ico-type-ppt.png' />";
	            else if (fileType == "xls" || fileType == "xlsx")
	                str += "<img src='../Style Library/LKKHPG/images/index/ico-type-xls.png' />";
	            else
	                str += "<img src='../Style Library/LKKHPG/images/index/ico-type-doucment.png' />";

	            //var index = fileName.lastIndexOf(".");
	            //if (index > -1)
	            //    fileName = fileName.substring(0, index);

							if (isMainFile == "1")
								str += "&nbsp;&nbsp;<span class='filename' title=" + fileName + ">" + fileName + "</span>";
							else
							str += "<input type='text' id='" + identity + "' value='" + fileName + "' style='height:25px' name='newAttachment' title=" + fileName.replace(/ /g, '_') + ">";


	            str += "&nbsp;&nbsp;<span class='rliang'>" + fileSize + "</span>";
	            str += "<a class='delete-file' style=\"cursor: pointer\" onclick=\"execDeleteAttachment('" + identity + "', '', '')\">";
	            str += "<img src=\"../Style Library/LKKHPG/images/index/ico-delete3.png\" />";
	            str += "</a>";
	            str += "</div>";

	            $("#filePicker" + identity).attr("file-name", fileName);
	            $("#filePicker" + identity).attr("file-type", fileType);

	            if (isMainFile == "1") {
	                $("#filePicker" + identity).attr("file-main", true);
	                $("#divFileMain").html(str);
	            } else {
								// console.log(gpName);
	                $("#filePicker" + identity).attr("file-main", false);
	                $("#" + gpName +" #divFileAttachment").append(str);
	            }
	      	}
	  	}

	    function execDeleteAttachment(identity, fileName, fileGuid) {

	        if (hasBeenLocked == true)
	            return;

	        $("#fileDiv" + identity).remove();
	        $("#filePicker" + identity).remove();

	        if (fileName != "") {
	            // var clientContext = new SP.ClientContext.get_current();
	            // var oList = clientContext.get_web().get_lists().getByTitle(listTitle);
	            // var item = oList.getItemById(spItemId);
	            // var attachmentFile = item.get_attachmentFiles().getByFileName(fileName);
	            // attachmentFile.deleteObject();
	            // clientContext.executeQueryAsync(
	            // function () {
	            //     $.ajax({
	            //         type: 'post',
	            //         url: siteUrl + "/_vti_bin/LKKHPG/KMService.svc/DeleteKMFileByGuid",
	            //         contentType: 'application/json;charset=utf-8',
	            //         dataType: 'json',
	            //         data: JSON.stringify({ fileGuid: fileGuid }),
	            //         success: function (data) {
	            //             var result = data.DeleteKMFileByGuidResult;
							//
	            //             if (result.ReturnCode != 0)
	            //                 alert(langattachments + result.ReturnMessage);
	            //         },
	            //         error: function (data) {
	            //             alert(langattachments);
	            //         }
	            //     });
	            // },
	            // function (sender, args) {
	            //     alert(langerror + args.get_message());
	            // });
				if (fileName.startsWith(currentFilePrefix()) && !isMainAttachmentMod)
					return;
							deleteFileQ.push(fileName);
							// deleteAttachment(fileName);
	        }
	    }

			function execRollBackKnowledgeOnSpSide(itemId) {
					// var clientContext = new SP.ClientContext.get_current();
					// var oList = clientContext.get_web().get_lists().getByTitle(listTitle);
					// var item = oList.getItemById(itemId);
					// item.deleteObject();
					// clientContext.executeQueryAsync(function () { }, function (sender, args) { });
			}

			function execUploadKnowledge() {

			    if ($.trim($("#txtKMTitle").val()) == "") {
		        alert(langtitle);
		        return;
			    }

			    if ($.trim($("#txtKMTitle").val()).length > 50) {
		        alert('标题太長，标题不能超过50字符');
		        return;
			    }

					// if( !( $("#category0").val() && $("#category1").val() && $("#category2").val() ) ){
					// 	alert('未有配给所有分类');
					// 	return;
					// }

			    // var mainDimensionCode = "";
			    // var aryDimension = $.makeArray($(".selected-dimension"));
			    // for (i = 0; i < aryDimension.length; i++) {
			    //     if ($.trim($(aryDimension[i]).val()) == "") {
			    //         alert(langknowledgedimension);
			    //         return;
			    //     }
			    //
			    //     var code = $(aryDimension[i]).attr("dimension-code");
			    //
			    //     if (code.startWith("0001"))
			    //         mainDimensionCode = code;
			    // }
			    //
			    // if (mainDimensionCode == "") {
			    //     alert(langprimary);
			    //     return;
			    // }
					//
					chooseAttachment = $("#contentSelect")[0].options[contentSelect.selectedIndex].value=='attachment' || $("#contentSelect")[0].options[contentSelect.selectedIndex].value=='video';
					if($("#contentSelect")[0].options[contentSelect.selectedIndex].value=='attachment'){
				    if ($.trim($("#divFileMain").text()) == "") {
				        alert(langmaster);
				        return;
				   }
					 var formatArr = ['pdf','xlsx','docx','pptx','xls','doc','ppt'];
					 if(formatArr.indexOf($(".filename")[0].innerText.split('.').pop() ) < 0 ) {
							alert('文档格式不正确');
							return;
						}
					}
					else if ($("#contentSelect")[0].options[contentSelect.selectedIndex].value=='video') {
						if ($.trim($("#divFileMain").text()) == "") {
				        alert(langmaster);
				        return;
				   }
					 var formatArr = ['mp4'];
					 if(formatArr.indexOf($(".filename")[0].innerText.split('.').pop() ) < 0 ) {
							alert('文档格式不正确');
							return;
						}
					}
					else {
						if ($("#rteeditor_editor").text().trim() == "" || $("#rteeditor_editor").text().trim() == "​"){
							alert('请填写正文内容');
							return;
						}
					}

			    $("#btnSave").attr("disabled", "disabled");

			    var clientContext = new SP.ClientContext.get_current();
			    var oList = clientContext.get_web().get_lists().getByTitle(listTitle);

			    var title = $.trim($("#txtKMTitle").val());
					var category0 = $("#category0").val();
					var category1 = $("#category1").val();
					var category2 = $("#category2").val();
					var inputTag = $("#txtKMTag div input");
					var tag = new Array();
					for(var i = 0; i < inputTag.length; i++)
					{
						if(inputTag[i].checked)
							tag.push(inputTag[i].value);
					}
					// console.log(tag);
			    this.oListItem = {};

			    // var lstCodes = [];
			    // for (i = 8; i <= mainDimensionCode.length; i += 4) {
			    //     lstCodes.push(mainDimensionCode.substr(0, i));
			    // }

			    // var dimensionCodePath = lstCodes.join("/");
			    var folderUrl = String.format("{0}/Lists/{1}", siteUrl, listTitle);

			    if (spItemId == 0) {
			        var itemCreateInfo = new SP.ListItemCreationInformation();
			        itemCreateInfo.set_folderUrl(folderUrl);
			        this.oListItem = oList.addItem(itemCreateInfo);
			        // oListItem.set_item('KMStatus', 2);
			    } else {
			        this.oListItem = oList.getItemById(spItemId);
			    }


					var LookupFieldValue = new SP.FieldLookupValue();
					var LookupFieldValue1 = new SP.FieldLookupValue();
					var LookupFieldValue2 = new SP.FieldLookupValue();
					LookupFieldValue.set_lookupId(category0);
					LookupFieldValue1.set_lookupId(category1);
					LookupFieldValue2.set_lookupId(category2);

			    oListItem.set_item('Title', title);
	        oListItem.set_item('Tag', tag);

					if(category0)
			    	oListItem.set_item('O', LookupFieldValue);
					else
						oListItem.set_item('O', "0;#None");
					if(category1)
			    	oListItem.set_item('First', LookupFieldValue1);
					else
						oListItem.set_item('First', "0;#None");
					if(category2)
			    	oListItem.set_item('Second', LookupFieldValue2);
					else
						oListItem.set_item('Second', "0;#None");

					oListItem.set_item('_Content',$("#rteeditor_editor").html());
					if(!spItemId)
						oListItem.set_item('userName',currentuserDisplayName);

					var material = '';

					for(var i = 0; i < $(".attachmentGroupName").length; i++)
						if(!$(".attachmentGroupName")[i].value)
							$(".attachmentGroupName")[i].value = '未知'+[i];

					if($("#divFileAttachment div input").length > 0)
					  for(var i = 0; i < $("#divFileAttachment div input").length; i++)
					    material += 'docName::'+$("#divFileAttachment div input")[i].value+'<,>'+$("#divFileAttachment div input")[i].title.trim().replace(/\+/g, "-")+'<,>'+$($("#divFileAttachment div input")[i]).parent().parent().parent().parent()[0].getElementsByClassName("attachmentGroupName")[0].value+';;' ;

					if($("#readingMaterial div input.urlName").length > 0)
					  for(var i = 0; i < $("#readingMaterial div input.urlName").length; i++)
							if($("#readingMaterial div input.urlName")[i].value && $("#readingMaterial div input.urlLink")[i].value)
					    	material += 'name::'+$("#readingMaterial div input.urlName")[i].value+'<,>'+ $("#readingMaterial div input.urlLink")[i].value+'<,>'+$($("#readingMaterial div input.urlLink")[i]).parent().parent().parent().parent()[0].getElementsByClassName("attachmentGroupName")[0].value+';;' ;

					oListItem.set_item('readingMaterials', material);

					var SelectedUser = new Array();
					var prefix = $(".choose_tip").attr('id');
	        for( var i = 0; i < lstSelectedUser4ViewOnly.length; i++){
						// console.log(str);
						if(lstSelectedUser4ViewOnly[i].EmailAddress == "")
							SelectedUser.push(SP.FieldUserValue.fromUser(lstSelectedUser4ViewOnly[i].ReceiverName));
						else{
							var str = prefix+lstSelectedUser4ViewOnly[i].Code.toLowerCase();
							SelectedUser.push(SP.FieldUserValue.fromUser(str));
						}
					}

								// console.log(SelectedUser);
					oListItem.set_item('Target', SelectedUser);


			    var mainFile = $(".selected-file[file-main=true]");
			    var mainFileName = mainFile.attr("file-name");
			    var mainFileType = mainFile.attr("file-type");

			    if (mainFileName != null && mainFileName != "") {
			        oListItem.set_item('FileName', mainFileName);
			        oListItem.set_item('FileType', mainFileType);
			    }

					var groupArr = '';
					for( var i = 0; i < $(".attachmentGroupName").length; i++)
						groupArr += $(".attachmentGroupName")[i].value+"<,>";
					oListItem.set_item('attachmentGroup',groupArr);
			    // $.each($.makeArray($(".selected-dimension")), function (i, item) {
			    //     var dimensionProperty = $(item).attr("dimension-property");
			    //     var dimensionCode = $(item).attr("dimension-code");
			    //     var dimensionFullName = $(item).attr("dimension-full-name");
			    //     var dimensionInfo =
			    //         {
			    //             DimensionPropertyName: dimensionProperty,
			    //             DimensionCode: dimensionCode
			    //         };
			    //     lstKMDimension.push(dimensionInfo);
			    //
			    //     oListItem.set_item(dimensionProperty, dimensionCode);
			    //     oListItem.set_item(String.format("{0}Name", dimensionProperty), dimensionFullName);
			    // });

				if (isMainAttachmentMod && !isMainAttachmentMod) {
					if (deleteFileQ.indexOf(mainFileName2) < 0){
						if (mainFileName2 != null && mainFileName2 != ""){
							deleteFileQ.push(mainFileName2);
						}
					}
				}
				if (!isMainAttachmentMod && ((chooseAttachment && mainFileName != null && mainFileName != "") || !chooseAttachment)) {
					currentDocVersion = swapCurrentDocVersion(currentDocVersion);
					isMainAttachmentMod = true;
				}
				if (currentDocVersion == null) {
					currentDocVersion = 0;
				}
				oListItem.set_item('CurrentActiveDoc', currentDocVersion);
				oListItem.set_item('IsMainAttachmentMod', isMainAttachmentMod);
				oListItem.set_item('KnowledgeType', $("#contentSelect").val());
				oListItem.set_item('ThumbnailData', $("#thumbnailPreview img#thumbnailData").attr('src'));

			    lockActionButtons(true);

			    oListItem.update();
			    clientContext.load(oListItem);
			    clientContext.executeQueryAsync(Function.createDelegate(this, this.onQuerySucceeded), Function.createDelegate(this, this.onQueryFailed));
			}

			function onQuerySucceeded() {
				deleteAttachment();
				// console.log(oListItem.get_id());
	      execUploadAttachment(oListItem.get_id());
	    }

	    function onQueryFailed(sender, args) {
	        lockActionButtons(false);
	        alert(languploadknowledge + args.get_message() + '\n' + args.get_stackTrace());
	    }

			function execUploadAttachment(itemId) {
					var aryFiles = $.makeArray($(".selected-file[file-uploaded=false]"));

					if (aryFiles.length > 0) {
							var fileControl = $(aryFiles[0]);
							var isMainFile = fileControl.attr("file-main");
							if (isMainFile && !chooseAttachment) {
								fileControl.attr("file-uploaded", true);
								execUploadAttachment(itemId);
							}
							var id = fileControl.attr("id").toString();
							var fileInput = document.getElementById(id);

							uploadFile(fileControl, itemId, fileInput, isMainFile);
					} else if(!spItemId) {
							//上传完成！
							// retrieveAttachmentProperties(itemId);
							console.log("update cid");

							$.ajax({
	      		      url: String.format("{0}/_api/web/lists/getByTitle('{1}')",
	                    siteUrl, listTitle),
	      		      type: "GET",
	      		      headers: {
	      		        'content-type': 'application/json;odata=verbose',
	      		          "accept": "application/json;odata=verbose",
	      		          "X-RequestDigest": $("#__REQUESTDIGEST").val()
	      		      },
	      		      success: function (res) {
	                  $.ajax({
	    								url: String.format("{0}/_api/web/lists/getByTitle('{1}')/items({2})",
	    										siteUrl, listTitle, itemId),
	    								type: "POST",
	    								data: JSON.stringify({  '__metadata': { 'type': res.d.ListItemEntityTypeFullName },
	    																				'cid': itemId
	    																			}),
	    								headers: {
	    									'content-type': 'application/json;odata=verbose',
	    										"accept": "application/json;odata=verbose",
	    										"X-RequestDigest": $("#__REQUESTDIGEST").val(),
	    										"IF-MATCH": "*",
	    										"X-HTTP-Method": "MERGE",

	    								},
	    								success: function (data) {
	    									lockActionButtons(false);
	    									$('#dialogUploadSucceed').modal({ backdrop: 'static' });
	    								},
	    								error: function (error) {
	                      console.log(error);
	    									alert("建立文档连结失败！");
							        }
							       });
	                 },
	      		      error: function (error) {
	      		        console.log(error);
	      		      }
			        });
					}
					else{
						lockActionButtons(false);
						$('#dialogUploadSucceed').modal({ backdrop: 'static' });
					}
			}

	    function uploadFile(fileControl, itemId, fileInput, isMainFile) {

	        // First get the file buffer and then send the request.
	        var file = fileInput.files[0];
	        var call = getFileBuffer(file);

	        call.done(function (arrayBuffer) {

	            //上传文件
	            var fileName = file.name;
							fileName = fileName.replace(/\+/g, "-").replace(/ /g, '_');
	            if (isMainFile == "true")
	                fileName = currentFilePrefix() + fileName;

	            $.ajax({
	                url: String.format("{0}/_api/web/lists/getByTitle('{1}')/items({2})/AttachmentFiles/add(FileName='{3}')",
	                    siteUrl, listTitle, itemId, fileName),
	                type: "POST",
	                async: false,
	                data: arrayBuffer,
	                processData: false,
	                headers: {
	                    "accept": "application/json;odata=verbose",
	                    "X-RequestDigest": $("#__REQUESTDIGEST").val()
	                },
	                success: function (data) {
	                    fileControl.attr("file-uploaded", true);
	                    console.log("upload file succeeed.");
	                    execUploadAttachment(itemId);
	                },
	                error: function (error) {
	                    //fileControl.attr("file-uploaded", false);
	                    lockActionButtons(false);
	                    console.log("upload file failed.");
	                    execRollBackKnowledgeOnSpSide(itemId);
	                    alert(languploadfile+'/r/n' + error.responseText);
	                    window.location.href = window.location.href;
	                }
	            });

	        });
	        call.fail(function (error) {
	            console.log("get file buffer failed.");
	            execRollBackKnowledgeOnSpSide(itemId);
	            alert(languploadfile+'/r/n' + error);
	            window.location.href = window.location.href;
	        });
	    }

			function getFileBuffer(file) {
	        var deferred = $.Deferred();
	        var reader = new FileReader();
	        reader.onloadend = function (e) {
	            deferred.resolve(e.target.result);
	        }
	        reader.onerror = function (e) {
	            deferred.reject(e.target.error);
	        }
	        reader.readAsArrayBuffer(file);

	        return deferred.promise();
	    }

			function retrieveAttachmentProperties(itemId) {
					var ctx = new SP.ClientContext.get_current();
					var web = ctx.get_web();
					var attachmentFolder = web.getFolderByServerRelativeUrl('Lists/' + listTitle + '/Attachments/' + itemId);
					var attachmentFiles = attachmentFolder.get_files();
					ctx.load(attachmentFiles);


					ctx.executeQueryAsync(function () {
							for (i = 0; i < attachmentFiles.get_count() ; i++) {
									var attachment = attachmentFiles.itemAt(i);
									var kmFile = {};
									var fileName = attachment.get_name()
									kmFile.FileName = fileName;

									if (fileName.indexOf(currentFilePrefix())>=0) {
											if ($(".selected-file[file-main='true']").length == 0)
													continue;

											kmFile.IsPrimaryFile = true;
									} else {
											if ($(".selected-file[file-name='" + fileName + "']").length == 0)
													continue;

											kmFile.IsPrimaryFile = false;
									}
									var parts = fileName.split('.');
									var fileType = parts[parts.length - 1];
									kmFile.FileType = fileType;

									kmFile.FileGuid = attachment.get_uniqueId().toString();
									kmFile.FileSize = attachment.get_length();
									kmFile.FileUrl = attachment.get_serverRelativeUrl();

									lstKMAttachment.push(kmFile);
							}
							lockActionButtons(false);
							$('#dialogUploadSucceed').modal({ backdrop: 'static' });
	            var filepath = $(".selected-file[file-main='true']").attr("file-name");
	            var fileType = filepath.replace(/.+\./, "").toLowerCase();
	            $("#spTips").text(filepath);
	            if (fileType == "doc" || fileType == "docx") {
	                $("#imgUploadNoticeIcon")[0].src = '../Style Library/LKKHPG/images/index/ico-type-doc.png'
	            }
	            else if (fileType == "xls" || fileType == "xlsx") {
	                $("#imgUploadNoticeIcon")[0].src = '../Style Library/LKKHPG/images/index/ico-type-xls.png'
	            }
	            else if (fileType == "ppt" || fileType == "pptx") {
	                $("#imgUploadNoticeIcon")[0].src = '../Style Library/LKKHPG/images/index/ico-type-ppt.png'
	            }
	            else if (fileType == "pdf") {
	                $("#imgUploadNoticeIcon")[0].src = '../Style Library/LKKHPG/images/index/ico-type-pdf.png'
	            }
	            else {
	                $("#imgUploadNoticeIcon")[0].src = '../Style Library/LKKHPG/images/index/ico-type-def.png'
	            }
					}, function (sender, args) {
							lockActionButtons(false);
							alert(langknowledge + args.get_message());
					});
			}

			function createExistedFileItem(knowledgeFile) {
	        var identity = new Date().getTime().toString();
	        var str = "<div class=\"fj_connent\" id='fileDiv" + identity + "'>";
	        var fileSize = bytesToSize(knowledgeFile.FileSize);
	        var fileType = knowledgeFile.FileType;

	        if (fileType == "doc" || fileType == "docx")
	            str += "<img src='../Style Library/LKKHPG/images/index/ico-type-doc.png' />";
	        else if (fileType == "def")
	            str += "<img src='../Style Library/LKKHPG/images/index/ico-type-def.png' />";
	        else if (fileType == "pdf")
	            str += "<img src='../Style Library/LKKHPG/images/index/ico-type-pdf.png' />";
	        else if (fileType == "ppt" || fileType == "pptx")
	            str += "<img src='../Style Library/LKKHPG/images/index/ico-type-ppt.png' />";
	        else if (fileType == "xls" || fileType == "xlsx")
	            str += "<img src='../Style Library/LKKHPG/images/index/ico-type-xls.png' />";
	        else
	            str += "<img src='../Style Library/LKKHPG/images/index/ico-type-doucment.png' />";

	        var fileName = knowledgeFile.FileName;
	        //var index = fileName.lastIndexOf(".");
	        //if (index > -1)
	        //    fileName = fileName.substring(0, index);

	        str += "&nbsp;&nbsp;<span class='filename' title=" + fileName + ">" + fileName + "</span>";
	        str += "&nbsp;&nbsp;<span class='rliang'>" + fileSize + "</span>";
	        str += "<a style=\"cursor: pointer\" onclick=\"execDeleteAttachment('" + identity + "', '" + knowledgeFile.FileName + "', '" + knowledgeFile.FileGuid.toString() + "')\">";
	        str += "<img src=\"../Style Library/LKKHPG/images/index/ico-delete3.png\" />";
	        str += "</a>";
	        str += "</div>";

	        if (knowledgeFile.IsPrimaryFile == true) {
	            $("#divFileMain").html(str);
	        } else {
	            $("#divFileAttachment").append(str);
	        }
	    }

			function contentSelection(val){
				// console.log(val);
				if (val.value=="richText"){
					$(".videoAtt").hide();
					$("#attachment").hide();
					$("#mainFileDiv").hide();
					$("#contentSelector").show();
				}
				else if (val.value=="video") {
					$(".videoAtt").show();
					$("#attachment").hide();
					$("#mainFileDiv").show();
					$("#contentSelector").hide();
				}
				else{
					$(".videoAtt").hide();
					$("#attachment").show();
					$("#mainFileDiv").show();
					$("#contentSelector").hide();
				}

			}

			function encodeImageFileAsURL(input) {
				var fileTypes = ['jpg', 'jpeg', 'png'];
				if (input.files && input.files[0]) {
		      var extension = input.files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
		          isSuccess = fileTypes.indexOf(extension) > -1;  //is extension in acceptable types

		      if (isSuccess) {
					  var file = input.files[0];
					  var reader = new FileReader();
					  reader.onloadend = function() {
					    // console.log('RESULT', reader.result);
							base64ToImg(reader.result);
							removeThumbnailAtt();
					  }
					  reader.readAsDataURL(file);
					}
					else {
						$("#thumbnailPreview").html("");
						removeThumbnailAtt();
						alert('请上传正确图片格式档');
					}
				}
			}

			function base64ToImg(result){
				$("#thumbnailPreview").html("<img id='thumbnailData' src='"+result+"' style='display:none'/>");
				$("#thumbnailPreview").append("<div style='background-image: url("+result+"); background-size: cover; background-position: center; width: 100px; height: 100px; margin: 20px 20px 0 0; display: inline-block;'/>");
				$("#thumbnailPreview").append('<a style="cursor: pointer; bottom: 2px; position: relative;" onclick="clearThumbnail()" ><img src="../Style Library/LKKHPG/images/index/ico-delete3.png" /></a>');
			}

			function clearThumbnail(){
				$("#thumbnailPreview").html("");
				removeThumbnailAtt();
			}

			function removeThumbnailAtt(){
				$("#divSelectFile #thumbnail").remove();
			}

			function addReadingMaterial(agId){
				for(var i = 0; i < $("input").length; i++)
					$("input")[i].setAttribute('value', $("input")[i].value);
				var identity = new Date().getTime().toString();
				// var arr = new Array();
				// if($("#"+agId+"#readingMaterial div input.urlName").length > 0)
				// 	for(var i = 0; i < $("#"+agId+" #readingMaterial div input.urlName").length; i++)
				// 		arr.push( {name : $("#"+agId+" #readingMaterial div input.urlName")[i].value, url : $("#"+agId+" #readingMaterial div input.urlLink")[i].value, identity : $("#"+agId+" #readingMaterial div")[i].agId});
				var str = $("#"+agId+" #readingMaterial").html()
				str += '<div id="'+identity+'" style="margin-bottom: 10px;"><span class="inline">名称 ： </span><input type="text" class="urlName"> - <span>链接 : </span><input type="text" class="urlLink"> <span style="color: red; cursor: pointer;" onclick="deleteReadingMaterial('+identity+')">  X  </span></div>';
				$("#"+agId+" #readingMaterial").html(str);

				// if(arr.length > 0)
				// 	for(var i = 0; i < arr.length; i++){
				// 		$("#"+agId+" #readingMaterial div")[i].agId = arr[i].identity;
				// 		$("#"+agId+" #readingMaterial div input.urlName")[i].value = arr[i].name;
				// 		$("#"+agId+" #readingMaterial div input.urlLink")[i].value = arr[i].url;
				// 	}

				for(var i = 0; i < $("input").length; i++)
					$("input")[i].setAttribute('value', $("input")[i].value);
			}

			function deleteReadingMaterial(identity){
				$("#"+identity).remove();
			}

			function addNewAttGp(name){
				var identity = new Date().getTime().toString();
				var agId = 'ag'+identity;
				// console.log(agId);
				for(var i = 0; i < $("input").length; i++)
					$("input")[i].setAttribute('value', $("input")[i].value);
				if(name)
					str = '<div id="'+agId+'" name="'+name+'">';
				else
					str = '<div id="'+agId+'">';
				str += '<div class="form-group clear_fix">';
				str += '<label class="col-sm-2 control-label upl doc">分类名称：</label> ';
				str += '<input type="text" class="form-control attachmentGroupName" style="width:auto; display: inline-block;"/> <span onclick="removeAttGp(\''+agId+'\')" style="color: red; cursor: pointer;">-</span></div>';
				str += '<div class="form-group clear_fix">';
				str += '<label class="col-sm-2 control-label upl doc">附件：</label>';
				str +=  '<div class="upfj">';
				str +=    '<img src="../Style Library/LKKHPG/images/index/up_load.png">';
				str +=    '<a style="cursor: pointer" onclick="popupFileChooser(0,\''+agId+'\')"> 上传附件</a>';
				str +=    '<span class="small_text_all">（附件单个最大50M）</span></div>';
				str +=  '<div id="divFileAttachment"><span style="height:5px">　<span></div></div>';
				str += '<div class="form-group clear_fix ">';
				str +=    '<label class="col-sm-2 control-label upl doc">链接：</label>';
				str +=   '<div style="margin-top: 6px;">';
				str += 			'<div><span class="hyperlink" onclick="addReadingMaterial(\''+agId+'\')">新增</span></div><br/></div>';
				str += '<div id="readingMaterial" class="col-sm-8"></div> </div></div>';

				// arr = new Array();
				// for(var i = 0; i < $(".attachmentGroupName").length; i++)
				// 	arr.push($(".attachmentGroupName")[i].value);

				$(".attachmentGroup").html( $(".attachmentGroup").html()+str);

				// for(var i = 0; i < arr.length; i++){
				// 	$(".attachmentGroupName")[i].value = arr[i];
				// }
			}

			function removeAttGp(agId){
				for(var i = 0; i < $('#'+agId+' input[name="attachment"]').length; i++){
					deleteFileQ.push( $('#'+agId+' input[name="attachment"]')[i].title );
				}
				$('#'+agId).remove();
			}

			function clearViewOnly(){
				lstSelectedUser4ViewOnly=[];
				$('#txtViewOnly').val('');
			}
	</script>

	<div class="frame_top">
	    <ul class="gs_xq">
	        <li onclick="backToList()">
	            <a href="###">审计站点</a>
	        </li>
	        <li class="spear"></li>
	        <li>文档编辑</li>

	    </ul>
	</div>

	<div class="frame_content">
	    <div class="whole_all_content">
	        <div class="form-group clear_fix ">
	            <label class="col-sm-2 control-label upl"><span class="red">*</span>标题：</label>
	            <div class="col-sm-6">
	                <input id="txtKMTitle" name="txtKMTitle" type="text" value="" class="form-control">
	            </div>
	        </div>
					<%-- <div id="divDimension">
						<div class="form-group clear_fix ">
								<label class="col-sm-2 control-label upl"><span class="red">*</span><asp:Literal ID="Literal4" runat="server" Text="<%$Resources:lkkhpg.portal,Dimensions%>"></asp:Literal>：</label>
								<div class="col-sm-6">
										<input dimension-control-id="10000" name="dimension" type="text" class="form-control selected-dimension"
												dimension-full-name=""
												dimension-display-name=""
												dimension-code=""
												dimension-property=""
												placeholder="<asp:Literal ID="Literal3513" runat="server" Text="<%$Resources:lkkhpg.portal,selectdimension1%>"></asp:Literal>" readonly='readonly'
												data-toggle="modal" data-target="#dialogSelectDimension" data-backdrop="static"
												onclick="resetDimensionSelectionValue(this)">
										<img src="../Style Library/LKKHPG/images/icon_wdplus.png" onclick="createDimensionTextBox()" />
								</div>
						</div>
					</div> --%>


					<div class="form-group clear_fix ">
	            <label class="col-sm-2 control-label upl">一级分类：</label>
	            <div class="col-sm-6">
	                <select id="category0" name="category0" class="form-control" onchange="get_lookupValue('First_Class')">
									</select>
	            </div>
	        </div>
					<div class="form-group clear_fix " id="cat1" style="display: none">
	            <label class="col-sm-2 control-label upl">二级分类：</label>
	            <div class="col-sm-6">
	                <select id="category1" name="category1" class="form-control" onchange="get_lookupValue('Second_Class')">
									</select>
	            </div>
	        </div>
					<div class="form-group clear_fix " id="cat2" style="display: none">
	            <label class="col-sm-2 control-label upl">三级分类：</label>
	            <div class="col-sm-6">
	                <select id="category2" name="category2" class="form-control">
									</select>
	            </div>
	        </div>
	        <div class="form-group clear_fix ">
	            <label class="col-sm-2 control-label upl">标签：</label>
	            <div class="col-sm-6">
	                <div id="txtKMTag"></div>
	            </div>
	        </div>
	        <div class="form-group clear_fix ">
	            <label class="col-sm-2 control-label upl">
				<asp:Literal ID="Literal6" runat="server" Text="<%$Resources:lkkhpg.portal,View%>"></asp:Literal>：</label>
	            <div class="col-sm-6">
	                <input id="txtViewOnly" name="txtViewOnly" type="text" value="" class="form-control" readonly="readonly" onclick="showSelectUserDialog(0)">
	                <span class="choose_person"><img src="../Style Library/LKKHPG/images/icon_athatt.png" onclick="showSelectUserDialog(0)"></span>
	                <span class="choose_tip">如果为空表示允许所有人可查看该知识</span>
									<span style="float:  right;" class="hyperlink" onclick="clearViewOnly()">清空</span>
	            </div>
	        </div>
					<%--
         <div class="form-group clear_fix ">
             <label class="col-sm-2 control-label upl"><asp:Literal ID="Literal7" runat="server" Text="<%$Resources:lkkhpg.portal,Edit%>"></asp:Literal>：</label>
             <div class="col-sm-6">
                 <input id="txtEditable" name="txtEditable" type="text" value="" class="form-control" readonly="readonly" onclick="showSelectUserDialog(1)">
                 <span class="choose_person"><img src="../Style Library/LKKHPG/images/icon_athatt.png"  onclick="showSelectUserDialog(1)"></span>
                 <span class="chose_tips"><asp:Literal ID="Literal33451" runat="server" Text="<%$Resources:lkkhpg.portal,EditTips%>"></asp:Literal></span>
             </div>
         </div>
         <div class="form-group clear_fix ">
             <label class="col-sm-2 control-label upl"><asp:Literal ID="Literal29" runat="server" Text="下载"></asp:Literal>：</label>
             <div class="col-sm-6">
                 <input id="txtDownloadable" name="txtDownloadable" type="text" value="" class="form-control" readonly="readonly" onclick="showSelectUserDialog(2)">
                 <span class="choose_person"><img src="../Style Library/LKKHPG/images/icon_athatt.png" onclick="showSelectUserDialog(2)"></span>
                 <span class="choose_tip">如果为空表示允许所有人可查看该知识</span>
             </div>
         </div>
         --%>

					<div class="form-group clear_fix ">
	            <label class="col-sm-2 control-label upl">类别：</label>
	            <div class="col-sm-6">
	                <div>
										<select id="contentSelect" onchange="contentSelection(this)" class="form-control">
										  <option value="attachment">附件</option>
										  <option value="richText">正文</option>
										  <option value="video">视频</option>
										</select>
									</div>
	            </div>
	        </div>

	        <div class="form-group clear_fix" id="mainFileDiv">
							<span id="attachment">
								<label class="col-sm-2 control-label upl doc"><span class="red">*</span>文档：</label>
		            <div class="upfj">
		                <img src="../Style Library/LKKHPG/images/index/up_load.png">
		                <a style="cursor: pointer" onclick="popupFileChooser(1,'main')">上传文档</a>
		                <span class="small_text_all">（可支持上传pdf、xlsx、doc、pptx格式文件）</span>
		            </div>
							</span>
							<span id="video" class="videoAtt" style="display:none;">
								<label class="col-sm-2 control-label upl doc"><span class="red">*</span>视频：</label>
		            <div class="upfj">
		                <img src="../Style Library/LKKHPG/images/index/up_load.png">
		                <a style="cursor: pointer" onclick="popupFileChooser(1,'main')">上传视频</a>
		                <span class="small_text_all">（可支持上传mp4格式文件，且大小不能超过50MB）</span>
		            </div>
							</span>
	            <div id="divFileMain">
	            </div>
	        </div>
					<div class="form-group clear_fix videoAtt" style="display:none;">
						<label class="col-sm-2 control-label upl doc">封面：</label>
						<div class="upfj">
							<img src="../Style Library/LKKHPG/images/index/up_load.png">
							<a style="cursor: pointer" onclick="thumbnailChooser()">上传封面</a>
							<span class="small_text_all">（建议图片大小为100x100）</span>
							<span class='rliang'> </span>
							<br/>
							<span id="thumbnailPreview"></span>
						</div>
					</div>
					<div class="form-group clear_fix " id="contentSelector" style="display:none;">
	            <label class="col-sm-2 control-label upl"><span class="red">*</span>正文內容：</label>
	            <div class="col-sm-6">
									<div id="rteeditor" class="form-control" style="height: auto;min-height: 100px;"><p></p></div>
	            </div>
	        </div>
					<div class="form-group clear_fix">
						<label class="col-sm-2 control-label upl doc">关联阅读分类：</label> <span onclick="addNewAttGp()" class="hyperlink" style="position: relative;top: 5px;">
						新增</span>
					</div>
					<div class="attachmentGroup">
						<%-- <div id="ag00">
							<div class="form-group clear_fix">
								<label class="col-sm-2 control-label upl doc">分类名称：</label>
									<input type="text" class="form-control attachmentGroupName" style="width:auto; display: inline-block;"/><span onclick="removeAttGp('ag00')" style="color: red; cursor: pointer;">
								-</span>
							</div>
			        <div class="form-group clear_fix">
			            <label class="col-sm-2 control-label upl doc">附件：</label>
			            <div class="upfj">
			                <img src="../Style Library/LKKHPG/images/index/up_load.png">
			                <a style="cursor: pointer" onclick="popupFileChooser(0,'ag00')">上传附件</a>
			                <span class="small_text_all">（附件单个最大50M）</span>
			            </div>
			            <div id="divFileAttachment"><span style="height:5px">　<span>
			            </div>
			        </div>
							<div class="form-group clear_fix ">
			            <label class="col-sm-2 control-label upl doc">链接：</label>
			            <div style="margin-top: 6px;">
											<div><span class="hyperlink" onclick="addReadingMaterial('ag00')">新增</span></div><br/>
			            </div>
									<div id="readingMaterial" class="col-sm-8">

									</div>
			        </div>
						</div>
						--%>
						</div>
						<%--上传维度分类背景--%>
						<%--<div class="form-group clear_fix ">
		           <label class="col-sm-2 control-label upl doc"><asp:Literal ID="Literal2459" runat="server" Text="<%$Resources:lkkhpg.portal,Background%>"></asp:Literal>：</label>
		           <div class="upfj">
		               <img src="../Style Library/LKKHPG/images/index/up_load.png">
		               <a style="cursor: pointer" onclick="" data-toggle="modal" data-target="#selectBg" data-backdrop="static"><asp:Literal ID="Literal30" runat="server" Text="<%$Resources:lkkhpg.portal,AddBg%>"></asp:Literal></a>
		               <%--<CMS:AssetUrlSelector runat="server" ID="AssetUrlSelector1" AssetUrlTextBoxVisible="false" AssetUrlClientID="selectBg" UseImageAssetPicker="true" ClientCallback="function(returnedUrl) { $('#wdBgImg').attr('src',returnedUrl);$('#selectBg').val(returnedUrl);}" />--%><%--  </div>
		           <div id="divBgPic">
		               <img src="/_layouts/15/LKKHPG.Common/images/index/user_pic_delete.png" class="delete_bg">
		               <img id="wdBgImg" src/>
		           </div>
		       </div>--%><%--上传维度分类背景--%>
					 <div class="form-group clear_fix tips">
		           <%--   <label class="col-sm-2 control-label upl doc"><asp:Literal ID="Literal16" runat="server" Text="<%$Resources:lkkhpg.portal,Tips%>"></asp:Literal>：</label>
		           <div class="col-sm-8">
		               <p><asp:Literal ID="Literal17" runat="server" Text="<%$Resources:lkkhpg.portal,infringement%>"></asp:Literal></p>
		               <p><asp:Literal ID="Literal18" runat="server" Text="<%$Resources:lkkhpg.portal,Maximumofeachfile%>"></asp:Literal></p>
		               <p><asp:Literal ID="Literal19" runat="server" Text="<%$Resources:lkkhpg.portal,supportsformats%>"></asp:Literal></p>
	            </div>--%>
	         </div>

					<div class="form-group clear_fix ">
	            <div class="btn_middle">
	                <span class="right_buttons">
	                    <button id="btnSave" type="button" class="btn fx btn-danger" onclick="execUploadKnowledge()">保存</button>
	                    <button id="btnCancel" type="button" class="btn fx btn-default" onclick="backToList()">取消</button>
	                </span>
	            </div>
	        </div>

					<%--
					<p class="red_kmText"><asp:Literal ID="Literal20" runat="server" Text="<%$Resources:lkkhpg.portal,confidentialdocuments%>"></asp:Literal></p>
 					<p class="red_kmText"><asp:Literal ID="Literal21" runat="server" Text="<%$Resources:lkkhpg.portal,privatedocuments%>"></asp:Literal></p>
  				--%>

	        <div id="divSelectFile" style="display: none"></div>
	    </div>
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolderBottom" runat="server">
	<%--上传维度背景--%><%-- <div class="modal fade" id="selectBg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width: 462px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">选择背景</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <a><img src="../Style Library/LKKHPG/images/index/cateloge_pic0.png" class="doc_bg_item"><img src="/_layouts/15/LKKHPG.PersonalSite/images/icon_chosen.png" class="chosen activeBg" style="display: none;"></a>
                        <a><img src="../Style Library/LKKHPG/images/index/cateloge_pic1.png" class="doc_bg_item"><img src="/_layouts/15/LKKHPG.PersonalSite/images/icon_chosen.png" class="chosen activeBg" style="display: none;"></a>
                        <a><img src="../Style Library/LKKHPG/images/index/cateloge_pic2.png" class="doc_bg_item"><img src="/_layouts/15/LKKHPG.PersonalSite/images/icon_chosen.png" class="chosen activeBg" style="display: none;"></a>
                        <a><img src="../Style Library/LKKHPG/images/index/cateloge_pic3.png" class="doc_bg_item"><img src="/_layouts/15/LKKHPG.PersonalSite/images/icon_chosen.png" class="chosen activeBg" style="display: none;"></a>
                        <a><img src="../Style Library/LKKHPG/images/index/cateloge_pic4.png" class="doc_bg_item"><img src="/_layouts/15/LKKHPG.PersonalSite/images/icon_chosen.png" class="chosen activeBg" style="display: none;"></a>
                        <a><img src="../Style Library/LKKHPG/images/index/cateloge_pic5.png" class="doc_bg_item"><img src="/_layouts/15/LKKHPG.PersonalSite/images/icon_chosen.png" class="chosen activeBg" style="display: none;"></a>
                    </div>
                    <input type="file" id="local_bg" value="上传图片">
                </div>

								<div class="modal-footer">
                     <button type="button" class="btn fx btn-default" data-dismiss="modal" id="cancelChooseBg"><asp:Literal ID="Literal31" runat="server" Text="<%$Resources:lkkhpg.portal,Cancel%>"></asp:Literal></button>
                    <button type="button" class="btn fx btn-danger" data-dismiss="modal" id="cofirmBg">确定</button>
                </div>

            </div>
        </div>
    </div>--%>
    <%--上传维度背景--%><div class="modal fade" id="dialogUploadSucceed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width: 320px;margin:  auto;text-align:  center;">
            <div class="modal-content">
                <div class="modal-body">

                    <div class="congra">
                        <p title="上传成功！">上传成功！</p>
                    </div>
                    <div class="docname">
                        <%--<div style="margin: 30px 0px 30px 10px">
                            <img src="../Style Library/LKKHPG/images/index/ico-type-doc.png" id="imgUploadNoticeIcon" /><span id="spTips" title="文档标题"></span></div> --%></div>
                </div>
                <div class="modal-footer" style="text-align: center;">
                    <button type="button" class="btn fx btn-danger" onclick="backToList()" data-dismiss="modal" style="color:#FFF; margin: 0px"><asp:Literal ID="Literal22" runat="server" Text="<%$Resources:lkkhpg.portal,Confirm%>"></asp:Literal></button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="dialogSelectDimension" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width: 593px">
            <div class="modal-content">
                <div class="modal-header">
                    <asp:Literal ID="Literal23" runat="server" Text="<%$Resources:lkkhpg.portal,SelectDimension%>"></asp:Literal>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">

                    <div class="chosen">
                        <span>
						<asp:Literal ID="Literal24" runat="server" Text="<%$Resources:lkkhpg.portal,Selected%>"></asp:Literal>
						:</span>
                        <span id="spDimensionPath" class="chosen_item"></span>
                    </div>
                    <!--选择维度-->
                    <div class="choosewd">
                        <span>
						<asp:Literal ID="Literal25" runat="server" Text="<%$Resources:lkkhpg.portal,SelectDimension%>"></asp:Literal>
						:</span>
                        <div id="divDimensionTypes" class="wd_item">
                        </div>
                    </div>
                    <!--树-->
                    <div class="wd_tree">
                        <div id="divTree" class="all_wd_tree">
                        </div>
                        <div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn fx btn-default" data-dismiss="modal">
					<asp:Literal ID="Literal26" runat="server" Text="<%$Resources:lkkhpg.portal,Cancel%>"></asp:Literal></button>
                    <button type="button" class="btn fx btn-danger" onclick="onDimensionSelected()">
					<asp:Literal ID="Literal27" runat="server" Text="<%$Resources:lkkhpg.portal,Yes%>"></asp:Literal></button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
