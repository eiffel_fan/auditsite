<%@ Page Language="C#" MasterPageFile="~site/_catalogs/masterpage/ThemeDefault.master" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage,Microsoft.SharePoint,Version=16.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:progid="SharePoint.WebPartPage.Document" %>

<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    案例详情
</asp:Content>

<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">
	<link href="/sites/KMsite/Style%20Library/LKKHPG/CSS/knowledgeCenter_frame.css" rel="stylesheet" />
  <style>
    .frame_top{
      padding: 0 45px;
      border-bottom: 0px;
    }
    .know_ledge_center{
      padding: 0 45px 45px;
    }
    #suiteBarDelta{
      position: relative;
      z-index: 2;
    }
    #s4-ribbonrow {
      z-index: 2;
    }
    .mainbody>.container{
      margin: 30px auto 55px!important;
      background-color: #f2f2f2;
    }
    .ui-dialog-titlebar, .ui-button, .ui-button:focus{
      background-color: #ff3e39;
      color: #FFF;
    }
    .know_ledge_right_1{
      width: 100%;
    }
    .gs_xq li:first-child {
      left: 0px;
    }
    #kmtitle{
      margin-bottom: 0px;
      padding-left: 0px;
      font-size: 26px;
    }
    #kmcreate{
      margin: 0 0 30px;
      font-size: 12px;;
      color: #57667E;
    }
    #kmattachments li, #kmattachments li a {
      font-size: 14px;
      color: #7795C0;
      height: 26px;
      line-height: 26px;
    }
    #kmattachments li a:hover{
      color: #FF5A5B;
    }
    #kmattachments li:last-child{
      margin-bottom: 10px;
    }
    #wordCount{
      position: relative;
      top: -3px;
      color: #B3B3B3;
    }
    #exp_btn{
      background-image: url(../SiteAssets/icon/icon_pdf1.png);
      width: 46px;
      height: 46px;
      background-size: cover;
      cursor: pointer;
      float: right;
      position: relative;
      top: -90px;
      right: 20px;
    }
    #exp_btn:hover{
      background-image: url("../SiteAssets/icon/icon_pdf1_sel.png");
    }
    .tj_button .btn{
      background: #FF5A5B;
      border: 1px solid #FF5A5B;
      border-radius: 2px;
      font-size: 14px;
      width: 120px;
      height: 40px;
    }
    .tj_button .btn:hover{
      background: #EC4849;
      border: 1px solid #EC4849;
    }
    .submitting{
      background: #999999!important;
      boarder: 1px solid #999999!important;
    }
    .popupWindow{
      width: 100%;
      height: 100%;
      background: rgba(34,34,34,0.22);
      position: fixed;
      top: 0;
      left: 0;
      display: none;
    }
    .popupBox{
      width: 360px;
      background: #FFFFFF;
      margin: auto;
      position: relative;
      top: 40%;
      text-align: center;
    }
    .popupContent{
      padding: 40px;
    }
    .popupContent>img{
      margin-bottom: 20px;
    }
    .popupContent>p{
      font-size: 18px;
    }
    .popupContent>span{
      font-sizr: 14px;
    }
    /* clock icon */
    a.icon {
      margin: 2px 5px;
      background-color: #FFFFFF;
      border: 1px solid #57667E;
      display: inline-block;
      position: relative;
      vertical-align: top;
      cursor: default;
    }
    a.icon:after,
    a.icon:before {
      background: #FFFFFF;
      border: 1px solid #57667E;
      content: '';
      position: absolute;
    }
    a.clock {
      border-radius: 100%;
      height: 10px;
      width: 10px;
    }
    a.clock:after,
    a.clock:before {
      border-left: none;
      width: 0;
    }
    a.clock:after {
      height: 4px;
      left: 4px;
      top: 1px;
    }
    a.clock:before {
      height: 3px;
      left: 5px;
      top: 3px;
      transform: rotate(90deg);
    }
    /* Strong Over Write index_frame.css*/
    strong span{
      vertical-align: unset;
    }
    strong{
      font-weight: 900;
    }
  </style>
  <div class="container">
      <div class="frame_top">
          <ul id="mainDim" class="gs_xq">
          </ul>
      </div>
      <div class="know_ledge_center"><hr style="margin: 0 0 26px;"/>
          <div id="kmtitle" class="know_top_h2"></div>
          <div class="know_ledge">
              <div class="all_link">
              <%--  <span id="kmauthor" class="all_link_name"></span> --%>
                <span id="kmcreate" class="all_link_time"></span>
              <%--   <div class="link_right_bar">
                      <span role="button" class="three_row_list" style="display: none" onclick="removeFavoriteLog();" id="disExistfav">
                          <img src="../Style Library/LKKHPG/images/index/scang_know.png" class="normal" /><img src="../Style Library/LKKHPG/images/index/scang_know_hover.png" class="active" /><asp:Literal ID="Literal18" runat="server" Text="<%$Resources:lkkhpg.portal,Favorited%>"></asp:Literal></span>
                      <span role="button" class="three_row_list" style="display: none" id="disNewfav" onclick="addKMtoMyFavorite();">
                          <img src="../Style Library/LKKHPG/images/index/scang_know.png" class="normal" /><img src="../Style Library/LKKHPG/images/index/scang_know_hover.png" class="active" /><asp:Literal ID="Literal1" runat="server" Text="<%$Resources:lkkhpg.portal,Favorite%>"></asp:Literal></span>
                      <span role="button" class="three_row_list" onclick="showSelectUserDialog()">
                          <img src="../Style Library/LKKHPG/images/index/tj_knowledge.png" class="normal" /><img src="../Style Library/LKKHPG/images/index/tj_knowledge_hover.png" class="active" /><asp:Literal ID="Literal2" runat="server" Text="<%$Resources:lkkhpg.portal,Recommend%>"></asp:Literal></span>
                      <span id="kmlikecount" role="button" class="three_row_list" onclick="likeKM();"></span>
                  </div> --%>
                  <div class="artcile_bottom" style="border: 0px solid; ">

                    <%--  <div class="uyl_all_list">
                          <span class="download_all">
                              <a id="kmfileurl" class="btn btn-danger" href="javascript:;" onclick="downloadKM();">
                                  <asp:Literal ID="Literal3" runat="server" Text="<%$Resources:lkkhpg.portal,Download1%>"></asp:Literal></a></span>
                          <span class="doc_tt_1">
                              <span>
                                  <asp:Literal ID="Literal4" runat="server" Text="<%$Resources:lkkhpg.portal,Size%>"></asp:Literal>：<strong id="kmfilesize"></strong></span>
                              <span class="sperline">|</span>
                              <span>
                                  <asp:Literal ID="Literal5" runat="server" Text="<%$Resources:lkkhpg.portal,Downloads%>"></asp:Literal>：<strong id="kmfiledownloadcount"></strong></span>
                              <span class="sperline">|</span>
                              <span>
                                  <asp:Literal ID="Literal6" runat="server" Text="<%$Resources:lkkhpg.portal,Score%>"></asp:Literal>：<a href="#feedbackAnchor"><span id="spanRating-show" /></span></a>
                              <span id="kmrating">0<asp:Literal ID="Literal17" runat="server" Text="<%$Resources:lkkhpg.portal,Score1%>"></asp:Literal>
                              </span>
                              <span id="kmcommentcount">(0<asp:Literal ID="Literal16" runat="server" Text="<%$Resources:lkkhpg.portal,Comments%>"></asp:Literal>)</span>
                              <span class="sperline">|</span>
                              <span>
                                  <asp:Literal ID="Literal7" runat="server" Text="<%$Resources:lkkhpg.portal,Pageviews%>"></asp:Literal>：<strong id="kmviewcount"></strong></span>
                          </span>
                      </div> --%>

                      <%-- <div style="background:#fff;z-index:999;position: absolute;top: 0px;margin-top:254px; height:86px;width:758px; border-bottom-width: 1px; border-bottom-style:solid; border-bottom-color:#E3E3E3">
                          <h1 id="fileTitle" class="text-center" style="font-size:x-large;padding-top:20px">
                          </h1>
                      </div> --%>
                      <div style="overflow: hidden;">
                        <iframe id="kmfileviewer" runat="server" src=""  style="min-height: 850px; width: 100%; top: -90px; position: relative; margin-bottom: -90px; display: none;" frameborder="no" border="0" marginwidth="0" marginheight="0" scrolling="no" allowtransparency="yes"></iframe>
                        <div id="exp_btn" style="display: none;" onclick="newWindow()"></div>
                      </div>
                      <div id="mainContent" style="text-align: center; display: none;"></div>
                      <div id="videoContent" style="display: none;"></div>
                  </div>
                  <div id="kmattachments" class="all_knowledge">
                  </div>
                  <hr style="width: 100%;"/>
                  <div class="zypj">
                    意见反馈： <span id="wordCount">(0/200)</span>
                  </div>
                  <div class="zypj">
                      <%-- <span class="know_ledge_left_1">
                          <asp:Literal ID="Literal9" runat="server" Text="<%$Resources:lkkhpg.portal,Comment%>"></asp:Literal>：</span>
		                  --%>
                      <span class="know_ledge_right_1">
                          <textarea class="form-control" id="txtFeedback" maxlength="200"></textarea></span>
                  </div>

                  <div class="tj_button">
                      <button type="button" class="btn btn-danger" id="btnSubmitFeedback" onclick="postComment()" disabled>
                          提交反馈
                          <%-- <asp:Literal ID="Literal14" runat="server" Text="<%$Resources:lkkhpg.portal,Submit%>"></asp:Literal> --%>
                      </button>
                  </div>

                  <%-- <div class="pl_button">
                      <p class="pl_p">
                          <asp:Literal ID="Literal15" runat="server" Text="<%$Resources:lkkhpg.portal,UserComments%>"></asp:Literal>（<span id="spanCommentCount"></span>）:
                      </p>
                      <div id="divCommentList">
                      </div>
                      <div class="page_all_list" id="commentPager" style="display: none;">
                          <ul>
                              <li>
                                  <a id="btnBack" href="javascript:;">
                                      <asp:Literal ID="Literal10" runat="server" Text="<%$Resources:lkkhpg.portal,Previouspage%>"></asp:Literal></a>
                              </li>
                              <li>
                                  <div class="black_color">
                                      <span id="pagenum">1</span>
                                  </div>
                              </li>
                              <li>
                                  <a id="btnNext" href="javascript:;">
                                      <asp:Literal ID="Literal13" runat="server" Text="<%$Resources:lkkhpg.portal,Nextpage%>"></asp:Literal></a>
                              </li>
                          </ul>
                      </div>

                  </div> --%>

              </div>

          </div>
          <%-- <div class="know_ledge_right">
              <WebPartPages:WebPartZone runat="server" Title="LKKHPGBodyRightZone" ID="LKKHPGBodyRightZone" FrameType="TitleBarOnly" PartChromeType="None" Width="100%">
                  <ZoneTemplate>
                  </ZoneTemplate>
              </WebPartPages:WebPartZone>
              </div> --%>
      </div>
    </div>
    <div class="popupWindow">
      <div class="popupBox">
        <div class="popupContent">
        </div>
      </div>
    </div>
  <script type="text/javascript">

        //Action Type ID and Name: 1-view,2-favorite,3-recommend,4-like,5-download,6-rating,7-comment
        var currentuserDisplayName;
        var currentuserLoginName;
        var currentuserID;
        var allowViewLogDuplicate = false;
        var allowDownloadLogDuplicate = false;
        var kmMainUrl = "";
        var langHome = "返回企业文库首页";
        var langNocommentyet = "暂无评论";
        var langPublishedby = "发布人：";
        var langDatePublished = "发布时间：";
        var langLike = "点赞";
        var langDownload = "下载";
        var langBatchDownload = "批量下载";
        var langcomment = "人评论）";
        var langViewDoc = "查看";
        var success = "评价成功！";
        // loadUserCompleteCallback = checkDocPrivilege;
        var lcid = SP.Res.lcid;
        switch (lcid) {
            case "1033":
                langHome = "Home";
                langNocommentyet = "No comment yet";
                langPublishedby = "Published by:";
                langDatePublished = "Date Published:";
                langLike = "Like";
                langDownload = "Download";
                langBatchDownload = "Batch Download";
                langcomment = "comment(s)）";
                success = "Comments Success!";
                langViewDoc = "View";
                break;
            case "1028":
                langHome = "返回企業文庫首頁";
                langNocommentyet = "暫無評論";
                langPublishedby = "發佈人：";
                langDatePublished = "發佈時間：";
                langLike = "讚好";
                langDownload = "下載";
                langBatchDownload = "批量下載";
                langcomment = "人評論）";
                success = "評論成功!";
                langViewDoc = "查看";
                break;
        }
        // var comment;
        var siteUrl = _spPageContextInfo.webAbsoluteUrl;
        var viewCount;
        var config = [];
        var listTitle = "";
        var atimer;
        var className = [];

        jQuery(document).ready(function () {
          $.ajaxSetup({ cache: false });
          jQuery(document).bind("contextmenu", function (e) {
              return false;
          });
          $("#txtFeedback").keyup(function (e) {
            $("#wordCount").html('('+($("#txtFeedback").val().length)+'/200)');
          });
          $(".popupWindow").click(function (e) {
            if(e.target.className=='popupWindow')
              $(".popupWindow").hide();
          });
          dataInti();
          // checkExistViewLog();
          // initKMViewCount();
          // initKMDownloadCount();
          // initKMLikeCount();
          // loadMyFavorite();
        });

        async function dataInti(){
    			await get_config();
          initUserInfo();
          initKMDetails();
    		}

        function get_config(){
    	    return $.ajax({
    	      url: String.format("{0}/_api/web/lists/getByTitle('app.Config')/items",
    	          siteUrl),
    	      type: "GET",
    	      headers: {
    	        'content-type': 'application/json;odata=verbose',
    	          "accept": "application/json;odata=verbose",
    	          "X-RequestDigest": $("#__REQUESTDIGEST").val()
    	      },
    	      success: function (data) {
    	        config = data.d.results;
    	        listTitle = config.filter(x=> x.Title == "MainListName")[0].Data;
              if(config.filter(x=> x.Title == "FeedBackList")[0].Data)
                $('#btnSubmitFeedback').prop('disabled',false);
    	      },
    	      error: function (error) {
    	        console.log(error);
    	      }
    	    });
    	  }

        function viewCountUpdate(){
          var itemId = getParameterByName("cid");
          var listTitle = "clickView";

          $.ajax({
            url: String.format("{0}/_api/web/lists/getByTitle('{1}')/items?$select=ID,viewCount,Title&$filter=cid eq "+itemId,
                siteUrl, listTitle),
            type: "GET",
            headers: {
              'content-type': 'application/json;odata=verbose',
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
            },
            success: function (data) {
              $.ajax({
                url: String.format("{0}/_api/web/lists/getByTitle('{1}')/items({2})",
                    siteUrl, listTitle, data.d.results[0].Id),
                type: "POST",
                data: JSON.stringify({  '__metadata': { 'type': 'SP.Data.ClickViewListItem' },
                                        'Title': data.d.results[0].Title,
                                        'viewCount': data.d.results[0].viewCount + 1
                                      }),
                headers: {
                  'content-type': 'application/json;odata=verbose',
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                    "IF-MATCH": "*",
                    "X-HTTP-Method": "MERGE",

                },
                success: function (data) {},
                error: function (error) {}
              });
            },
            error: function (error) {}
          });
        }

        //Get User Info
        function initUserInfo() {
            var context = new SP.ClientContext.get_current();
            this.website = context.get_web();
            this.currentUser = website.get_currentUser();
            context.load(currentUser);
            context.executeQueryAsync(Function.createDelegate(this, this.onQueryUserSucceeded), Function.createDelegate(this, this.onQueryUserFailed));
        }

        function onQueryUserSucceeded(sender, args) {
            currentuserLoginName = currentUser.get_loginName();
            currentuserDisplayName = currentUser.get_title();
            currentuserID = currentUser.get_id();
        }

        function onQueryUserFailed(sender, args) {

        }

        //初始化知识信息
        function initKMDetails() {
            var itemid = getParameterByName("cid");
            if (itemid != null) {
                if (itemid != "") {
                    var clientContext = SP.ClientContext.get_current();
                    var oList = clientContext.get_web().get_lists().getByTitle(listTitle);
                    this.oListItem = oList.getItemById(itemid);
                    clientContext.load(oListItem);
                    clientContext.executeQueryAsync(Function.createDelegate(this, this.onGetKMSucceeded), Function.createDelegate(this, this.onGetKMFailed));
                }
            }
        }

        function onGetKMSucceeded() {
            var title = oListItem.get_item('Title');
            var kmtags = oListItem.get_item('Tag');

            var Oclass = oListItem.get_item("O");
            var first = oListItem.get_item("First");
            var second = oListItem.get_item("Second");
            var _Content = oListItem.get_item("_Content");
            var knowledgeType = oListItem.get_item("KnowledgeType");
            var thumbnailData = oListItem.get_item("ThumbnailData");
            // viewCount =  oListItem.get_item("viewCount");
            // comment = oListItem.get_item("comment");
            // console.log(comment);
            let firstLookupValue = first.get_lookupValue();
            let secondLookupValue = second.get_lookupValue();
            if(firstLookupValue)
              firstLookupValue = ' ＞ ' + firstLookupValue;
            else   firstLookupValue = '';
            if(secondLookupValue)
              secondLookupValue = ' ＞ ' + secondLookupValue;
            else secondLookupValue = '';

            className.push(Oclass.get_lookupValue());
            className.push(first.get_lookupValue());
            className.push(second.get_lookupValue());

            $("#mainDim").html('<li>' + Oclass.get_lookupValue() + firstLookupValue + secondLookupValue + '</li>');
            // if (kmtags != null) {
            //     kmtags = "标签：" + kmtags;
            // }
            // else {
                kmtags = "";
            // }
            var kmauthor = oListItem.get_item('Author').get_lookupValue();
            var kmcreated = oListItem.get_item('Modified').format("yyyy-MM-dd");
            var rawTxt = oListItem.get_item("readingMaterials");
            var splited = '';
            if(rawTxt)
              splited = rawTxt.split(";;")

            //
            jQuery("#kmtitle").html("<span class='titleName'>" + HtmlUtil.htmlEncode(title) + "</span><span class='bqiao'>" + HtmlUtil.htmlEncode(kmtags) + "</span>");
            jQuery("#kmauthor").html(langPublishedby + kmauthor);
            // jQuery("#kmcreate").html(langDatePublished + kmcreated);
            jQuery("#kmcreate").html('<a class="clock icon" title="Clock" href="#"></a>' + kmcreated);

            //initKMDimensionData();

            var attachmentGroup = oListItem.get_item("attachmentGroup");
            if(attachmentGroup){
              $("#kmattachments").append('<hr/>');
              var gpName = attachmentGroup.split("<,>");
              for( var i = 0; i < gpName.length-1; i++){
                $("#kmattachments").append('<div id="'+gpName[i].replace(/ /g,"_")+'"><h2>'+gpName[i]+'</h2><ul></ul></div>');
              }
            }
            // console.log(attachmentGroup);
            if(knowledgeType=='attachment'){
              jQuery("#ctl00_PlaceHolderMain_kmfileviewer").show();
            }
            else if (knowledgeType=='video') {
              jQuery("#videoContent").show();
            }
            else{
              jQuery("#mainContent").show();
              jQuery("#mainContent").html(_Content);
            }

            initAttachments(attachmentGroup,splited);
            viewCountUpdate();
        }

        function onGetKMFailed(sender, args) {
            alert("您没有当前文档的访问权限");
            history.go(-1);
            // alert('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
        }
      function swapCurrentDocVersion(curr) {
				return (parseInt(curr) + 1) % 2;
			}

			function mainItemName(curr) {
				return parseInt(curr) == 0 ? 'MAIN' : 'MAIN2';
			}

			function filePrefix(curr) {
				return '[' + mainItemName(curr) + ']';
			}

        function initAttachments(attachmentGroup,splited){
          var itemid = getParameterByName("cid");
          if (itemid != null) {
              if (itemid != "") {
                  var clientContext = SP.ClientContext.get_current();
                  var oList = clientContext.get_web().get_lists().getByTitle(listTitle);
                  var oListItem = oList.getItemById(itemid);
                  var currActive = oListItem.get_item('CurrentActiveDoc');
                  var currFilePrefix = filePrefix(currActive);
                  this.attachmentFiles = oListItem.get_attachmentFiles();
                  clientContext.load(attachmentFiles);
                  clientContext.executeQueryAsync(function () {
                      var attachmentcount = 0;
                      var attachmentItemHtml = "";
                      var fileItemEnumerator = attachmentFiles.getEnumerator();
                      while (fileItemEnumerator.moveNext()) {
                          var ofileItem = fileItemEnumerator.get_current();
                          var fileurl = ofileItem.get_serverRelativeUrl();
                          var filename = ofileItem.get_fileName();
                          if (filename.indexOf(currFilePrefix) >= 0) {
                              //jQuery("#kmfileurl").attr("href", fileurl);

                              kmMainUrl = fileurl;
                              if (filename.indexOf("pdf") > 0 || filename.indexOf("doc") > 0 || filename.indexOf("docx") > 0 || filename.indexOf("xls") > 0
                              || filename.indexOf("xlsx") > 0 || filename.indexOf("ppt") > 0 || filename.indexOf("pptx") > 0) {
                                  //jQuery("#ct100_PlaceHolderMain_kmfileviewer").attr("src", "/_layouts/15/WopiFrame.aspx?sourcedoc=" + encodeURI(fileurl) + "&action=default");
                                  jQuery("#ctl00_PlaceHolderMain_kmfileviewer").attr("src", _spPageContextInfo.webAbsoluteUrl + "/_layouts/15/wopiFrame.aspx?sourcedoc=" + encodeURI(fileurl) + "&action=view");
                                  jQuery("#exp_btn").show();
                              }
                              else if(filename.indexOf("mp4") > 0 ){
                                  jQuery("#videoContent").html('<video controls autoplay style="width: 100%; "><source src="'+encodeURI(fileurl)+'" type="video/mp4">Your browser does not support HTML5 video.</video>');
                              }
                              else {
                                  // jQuery("#ctl00_PlaceHolderMain_kmfileviewer").hide();
                                    jQuery("#ctl00_PlaceHolderMain_kmfileviewer").attr("src",_spPageContextInfo.webAbsoluteUrl + "/Lists/"+ listTitle +"/Attachments/"+ itemid +"/" + filename);
                              }

                              getKMFileByServerRelativeUrl(fileurl);
                          }
                          else {
                            var viewFileUrl;
                            if (filename.indexOf("pdf") > 0 || filename.indexOf("doc") > 0 || filename.indexOf("docx") > 0 || filename.indexOf("xls") > 0
                            || filename.indexOf("xlsx") > 0 || filename.indexOf("ppt") > 0 || filename.indexOf("pptx") > 0) {
                              // var downloadFileUrl  = _spPageContextInfo.webAbsoluteUrl + "/_layouts/15/download.aspx?sourceurl=" + encodeURI(fileurl);
                              viewFileUrl = _spPageContextInfo.webAbsoluteUrl + "/_layouts/15/wopiFrame.aspx?sourcedoc=" + encodeURI(fileurl) + "&action=view";
                            }
                            else {
                              viewFileUrl = _spPageContextInfo.webAbsoluteUrl + "/Lists/"+ listTitle +"/Attachments/"+ itemid +"/" + filename;
                            }
                              // attachmentItemHtml += "<div class=\"cell_td_small\"><span class=\"left_cell_bb\">" +
                              //                         filename + "</span><span class=\"right_cell_aa\"> <span class=\"wen_tt\"></span> <a href=\"" + downloadFileUrl + "\" target=\"_blank\" class=\"edit_all_1 downloadattach\">" +
                              //                         "<img src=\"../Style Library/LKKHPG/images/index/ico-download1.png\" class=\"normal\"><img src=\"../Style Library/LKKHPG/images/index/ico-download2.png\" class=\"active\">" + langDownload + "</a></span>" +
                              //                       "</div>";
                              for( var i = 0; i < splited.length; i++){
                                if( filename == splited[i].split("<,>")[1]){
                                  var targetId = splited[i].split("<,>")[2].replace(/ /g,"_");
                                  $("#"+targetId+" ul").append("<li style='list-style: disc; margin-left: 20px;'><a href=\"" + viewFileUrl + "\" target=\"_blank\" class=\"edit_all_1 downloadattach\">"+splited[i].split("<,>")[0].split("::")[1]+"</a></li>");
                                }
                              }
                              attachmentcount++;
                          }
                      }
                      $("#ctl00_PlaceHolderMain_kmfileviewer").attr;
                      jQuery("#kmattachments").append(attachmentItemHtml);

                      for( var i = 0; i < splited.length; i++){
                        if( splited[i].split("<,>")[0].split("::")[0] == 'name'){
                          var targetId = splited[i].split("<,>")[2].replace(/ /g,"_");
                          $("#"+targetId+" ul").append("<li style='list-style: disc; margin-left: 20px;'><a href=\"" + splited[i].split("<,>")[1] + "\" target=\"_blank\" class=\"edit_all_1 downloadattach\">"+splited[i].split("<,>")[0].split("::")[1]+"</a></li>");
                        }
                      }
                  }, Function.createDelegate(this, this.onGetKMAttachesFailed));
              }
          }
        }

        //获取Request参数
        function getParameterByName(name) {
            name = name.replace(/[[]/, "[").replace(/[]]/, "]");

            var regexS = "[?&]" + name + "=([^&#]*)";
            var regex = new RegExp(regexS);
            var results = regex.exec(window.location.href);

            if (results == null)
                return "";
            else
                return decodeURIComponent(results[1]);
        }

        function getKMFileByServerRelativeUrl(fileServerRelativeUrl) {
            var userid = _spPageContextInfo.userId;
            var requestUri = _spPageContextInfo.webAbsoluteUrl + "/_api/web/getfilebyserverrelativeurl('" + fileServerRelativeUrl + "')";
            var requestHeaders = { "accept": "application/json;odata=verbose" };
            $.ajax({
                url: requestUri,
                contentType: "application/json;odata=verbose",
                headers: requestHeaders,
                success: onSuccess,
                error: onError
            });
        }

        function onSuccess(data, request) {
            var size = data.d.Length;
            var displaysize = "";
            if (size > 1024) {
                size = size / 1024 / 1024;
                displaysize = size.toFixed(1).toString() + "M";
            }
            jQuery("#kmfilesize").html(displaysize);
        }

        function onError(error) {}

        function insertExceptionLog(exceptionTitle, exceptionFunName, exceptionMsg) {}

        //Create A Comment Record
        function postComment(){
          if($("#txtFeedback").val().trim()==""){
            $( ".popupContent" ).html('<img src="../SiteAssets/icon/icon_fall.png"/><p>提交失败！</p><span>请填写意见内容</span>');
            $( ".popupWindow" ).show();
            setTimeout(function(){ $( ".popupWindow" ).hide();}, 3000);
          }
          else if($("#txtFeedback").val().trim().length>200){
            $( ".popupContent" ).html('<img src="../SiteAssets/icon/icon_fall.png"/><p>提交失败！</p><span>您已超文字数量</span>');
            $( ".popupWindow" ).show();
            setTimeout(function(){ $( ".popupWindow" ).hide();}, 3000);
          }
          else {
            $("#btnSubmitFeedback").text("提交中…");
            $("#btnSubmitFeedback").addClass('submitting');
            var itemid = getParameterByName("cid");
            var listTitle = config.filter(x=> x.Title == "FeedBackList")[0].Data;

          $.ajax({
              url: String.format("{0}/_api/SP.UserProfiles.PeopleManager/GetMyProperties", siteUrl),
              type: "GET",
              headers: {
                  "accept": "application/json;odata=verbose",
              },
              success: function (userResults) {

                  var department;
                  var properties = userResults.d.UserProfileProperties.results;
                  for (var i = 0; i < properties.length; i++) {
                    if (properties[i].Key == "Department") {
                      department = properties[i].Value;
                    }
                  }
                    $.ajax({
                      url: String.format("{0}/_api/web/lists/getByTitle('{1}')/items",
                          siteUrl, listTitle),
                      type: "POST",
                      data: JSON.stringify({  '__metadata': { 'type': 'SP.Data.'+listTitle+'ListItem' },
                                              'Title': $("#kmtitle .titleName").text(),
                                              'URL': siteUrl+'/SitePages/Audit_Detail.aspx?cid='+itemid,
                                              'O': className[0],
                                              'first': className[1],
                                              'second': className[2],
                                              'content': $("#txtFeedback").val(),
                                              'userName': currentuserDisplayName,
                                              'department': department
                                            }),
                      headers: {
                        'content-type': 'application/json;odata=verbose',
                          "accept": "application/json;odata=verbose",
                          "X-RequestDigest": $("#__REQUESTDIGEST").val()
                      },
                      success: function (data) {
                          console.log("update succeeed.");
                          $("#txtFeedback").val("");
                          $("#wordCount").html('(0/200)');
                          $("#btnSubmitFeedback").removeClass('submitting');
                          $("#btnSubmitFeedback").text("提交反馈");
                          $( ".popupContent" ).html('<img src="../SiteAssets/icon/icon_success.png"/><br/><br/>提交成功!');
                          $( ".popupWindow" ).show();
                          setTimeout(function(){ $( ".popupWindow" ).hide(); }, 3000);
                      },
                      error: function (XMLHttpRequest) {
                        $("#btnSubmitFeedback").removeClass('submitting');
                        $("#btnSubmitFeedback").text("重新提交");
                        $( ".popupContent" ).html('<img src="../SiteAssets/icon/icon_fall.png"/><p>提交失敗!</p>');
                        $( ".popupWindow" ).show();
                        setTimeout(function(){ $( ".popupWindow" ).hide(); }, 3000);
                      }
                    })
                  }  ,
                error: function (XMLHttpRequest) {
                  if(XMLHttpRequest.readyState == 0){
                    // Network error
                    $("#btnSubmitFeedback").removeClass('submitting');
                    $("#btnSubmitFeedback").text("重新提交");
                    $( ".popupContent" ).html('<img src="../SiteAssets/icon/icon_fall.png"/><p>提交失敗!</p><span>找不到网络</span>');
                    $( ".popupWindow" ).show();
                    setTimeout(function(){ $( ".popupWindow" ).hide(); }, 3000);
                  }
                }
            });
          }
        }

        //Others
        function newWindow(){
          window.open($("#ctl00_PlaceHolderMain_kmfileviewer").attr('src'));
        }
  </script>
</asp:Content>
